/**
Lenny SIEMENI 1055234
Mehran ASADI 1047847
 */
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FAT_NAME_LENGTH 11  /** Longueur d'un nom fichier en FAT qui est 11 charactere */
#define FAT_EOC_TAG 0x0FFFFFF8  /** End Of Chain Tag */
#define FAT_DIR_ENTRY_SIZE 32   /** Size of d'une entrÃƒÂ©e d'un root directory en FAT32 qui est 32 bit */
#define HAS_NO_ERROR(err) ((err) >= 0) /** Macro pour tester les erreurs */
#define NO_ERR 0
#define GENERAL_ERR -1
#define OUT_OF_MEM -3
#define RES_NOT_FOUND -4
#define CAST(t, e) ((t) (e))
/** Transformer un tableau de byte en une valeur selon le low-indian */
#define as_uint16(x) \
((CAST(uint16,(x)[1])<<8U)+(x)[0])
#define as_uint32(x) \
((((((CAST(uint32,(x)[3])<<8U)+(x)[2])<<8U)+(x)[1])<<8U)+(x)[0])

typedef unsigned char uint8;
typedef uint8 bool;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef int error_code;


/**
 * Pourquoi est-ce que les champs sont construit de cette faÃƒÂ§on et non pas directement avec les bons types?
 * C'est une question de portabilitÃƒÂ©. FAT32 sauvegarde les donnÃƒÂ©es en BigEndian, mais votre systÃƒÂ¨me de ne l'est
 * peut-ÃƒÂªtre pas. Afin d'ÃƒÂ©viter ces problÃƒÂ¨mes, on lit les champs avec des macros qui convertissent la valeur.
 * Par exemple, si vous voulez lire le paramÃƒÂ¨tre BPB_HiddSec et obtenir une valeur en entier 32 bits, vous faites:
 *
 * BPB* bpb;
 * uint32 hidden_sectors = as_uint32(BPB->BPP_HiddSec);
 *
 */
typedef struct BIOS_Parameter_Block_struct {
    uint8 BS_jmpBoot[3];      /** L'instruction jmp au dÃƒÂ©but du code. Permet au processeur de sauter par dessus les donnÃƒÂ©es */
    uint8 BS_OEMName[8];      /** Le nom du systÃƒÂ¨me */
    uint8 BPB_BytsPerSec[2];  /** 512, 1024, 2048 or 4096      ==> le nombre de bytes dans un secteur */
    uint8 BPB_SecPerClus;    /** 1, 2, 4, 8, 16, 32, 64 or 128 ==> le nombre de secteurs dans un cluster */
    uint8 BPB_RsvdSecCnt[2]; /** 1 for FAT12 and FAT16, typically 32 for FAT32 ==> le nombre de secteur rÃƒÂ©servÃƒÂ©s (pour ce que l'on veut) */
    uint8 BPB_NumFATs;       /** should be 2 ==> Le nombre de tables FAT (que l'on verra par aprÃƒÂ¨s) */
    uint8 BPB_RootEntCnt[2]; /** Le nombre d'entrÃƒÂ©es root (0 en FAT32) */
    uint8 BPB_TotSec16[2];   /** Le nombre total de secteurs (pas en FAT32) */
    uint8 BPB_Media;         /** Le type de mÃƒÂ©dia physique (0xF8 pour un disque dur) */
    uint8 BPB_FATSz16[2];    /** Le nombre de secteur pour une table FAT */
    uint8 BPB_SecPerTrk[2];  /** Le nombre de secteur par track */
    uint8 BPB_NumHeads[2];   /** Le nombre de tÃƒÂªtes */
    uint8 BPB_HiddSec[4];    /** Le nombre de secteurs cachÃƒÂ©s (on assumera 0) */
    uint8 BPB_TotSec32[4];   /** Le nombre total de secteurs, cette fois pour FAT 23 "nb_logical_sectors" */
    uint8 BPB_FATSz32[4];    /** La taille d'une table FAT (pour fat 32) "nb_sectors_per_fat"*/
    uint8 BPB_ExtFlags[2];   /** On ignore ce champ */
    uint8 BPB_FSVer[2];      /** La version du systÃƒÂ¨me de fichier "fs_version"*/
    uint8 BPB_RootClus[4];   /** Le cluster de la table du dossier root (ici on assumera que c'est toujours 2) "first_cluster_root_dir"*/
    uint8 BPB_FSInfo[2];     /** La version (on ignore) */
    uint8 BPB_BkBootSec[2];  /** backup_bootsec */
    uint8 BPB_Reserved[12];  /** reserved_data */
    uint8 BS_DrvNum;        // On ignore
    uint8 BS_Reserved1;    // On ignore (rÃƒÂ©servÃƒÂ©)
    uint8 BS_BootSig;      // On ignore
    uint8 BS_VolID[4];    // On ignore
    uint8 BS_VolLab[11];  // On ignore
    uint8 BS_FilSysType[8]; /** Une chaÃƒÂ®ne de caractÃƒÂ¨re qui devrait dire FAT32 */
} BPB;

typedef struct FAT_directory_entry_struct {   /** Nom	            Largeur(bytes) */
    uint8 DIR_Name[FAT_NAME_LENGTH];          /** Nom du fichier	    11 */
    uint8 DIR_Attr;                          /** Attributs du fichier	1  */
    uint8 DIR_NTRes;                         /** ReservÃƒÂ©	            1 */
    uint8 DIR_CrtTimeTenth;                  /** 10iem de secondes  	1 */
    uint8 DIR_CrtTime[2];                   /** Heure de crÃƒÂ©ation	     2 */
    uint8 DIR_CrtDate[2];                   /** Date de crÃƒÂ©ation	     2  */
    uint8 DIR_LstAccDate[2];               /** Date du dernier accÃƒÂ¨s 	  2 */
    uint8 DIR_FstClusHI[2];                /** Partie haute du premier cluster	2 */
    uint8 DIR_WrtTime[2];                 /** DerniÃƒÂ¨re heure d'ÃƒÂ©criture     	2 */
    uint8 DIR_WrtDate[2];                 /** DerniÃƒÂ¨re date d'ÃƒÂ©criture      	2 */
    uint8 DIR_FstClusLO[2];              /** Partie basse du premier cluster	2 */
    uint8 DIR_FileSize[4];              /** Taille du fichier	4 */
} FAT_entry;
uint16 clusterCourant;
static BPB bpbinfo;
uint8 ilog2(uint32 n) {
    uint8 i = 0;
    while ((n >>= 1U) != 0)
        i++;
    return i;
}

//--------------------------------------------------------------------------------------------------------
//                                           DEBUT DU CODE
//--------------------------------------------------------------------------------------------------------
/**
 * Exercice 1
 * Cette fonction prend un numÃƒÂ©ro de cluster et retourne le LBA (logical block address) qui correspond
 * au secteur Ãƒ  lire. La conversion doit sÃ¢â‚¬â„¢effectuer selon la spÃƒÂ©cification pour que la lecture de fichier
 * soit correcte. De plus, le "BIOS Parameter Block" (BPB) doit ÃƒÂªtre utilisÃƒÂ© pour tenir compte de la
 * gÃƒÂ©omÃƒÂ©trie du disque. LÃ¢â‚¬â„¢algorithme de conversion est donnÃƒÂ© verbatim dans notre document.
 * Prend cluster et retourne son addresse en secteur dans l'archive
 * @param block le block de paramÃƒÂ¨tre du BIOS
 * @param cluster le cluster Ãƒ  convertir en LBA
 * @param first_data_sector le premier secteur de donnÃƒÂ©es, donnÃƒÂ©e par la formule dans le document
 * @return le LBA
 */
uint32 cluster_to_lba(BPB *block, uint16 cluster, uint32 first_data_sector) {
    /** lba($cluster) = $begin + ($cluster - $root-entry) * $sectors-per-clusters */
    return first_data_sector + (cluster - FAT_DIR_ENTRY_SIZE) * as_uint32(block->BPB_BytsPerSec);
}

/**
 * Exercice 2
 * Cette fonction prend en paramÃƒÂ¨tre (entre autre) un numÃƒÂ©ro de cluster. Cette fonction doit placer
 * dans le pointeur value le cluster qui suit le cluster courant. Cette information se trouve dans la table
 * FAT du disque. Portez attention Ãƒ  bien calculer lÃ¢â‚¬â„¢adresse en byte du cluster courant. Si vous faites
 * une erreur, le mauvais cluster sera lu et la lecture de fichier sera impossible. Il nÃ¢â‚¬â„¢est pas nÃƒÂ©cÃƒÂ©ssaire de
 * vÃƒÂ©rifier lÃ¢â‚¬â„¢information avec la deuxiÃƒÂ¨me table FAT, seule la premiÃƒÂ¨re suffira ici.
 * Va chercher une valeur dans la cluster chain
 * @param block le block de paramÃƒÂ¨tre du systÃƒÂ¨me de fichier
 * @param cluster le cluster qu'on veut aller lire
 * @param value un pointeur ou on retourne la valeur
 * @param archive le fichier de l'archive
 * @return un code d'erreur
 */
error_code get_cluster_chain_value(BPB *block,
                                   uint16 cluster,
                                   uint16 *value,
                                   FILE *archive) {

    if (cluster == 0)
        cluster = 2;
    uint32 offset =  ((cluster - 2) * as_uint16(bpbinfo.BPB_BytsPerSec)) + (as_uint16(bpbinfo.BPB_BytsPerSec) * as_uint16(bpbinfo.BPB_RsvdSecCnt)) + (bpbinfo.BPB_NumFATs * as_uint32(bpbinfo.BPB_FATSz32) * as_uint16(bpbinfo.BPB_BytsPerSec));
    fseek(archive, offset + 26, SEEK_SET);
    fread(value, 2, 1, archive);
    return 0;
}


/**
 * Exercice 3
 * Le but de cette fonction est de comparer un nom de fichier ou de dossier FAT avec un nom de fichier
 * entrÃƒÂ© par un utilisateur. Attention ! Il ne sÃ¢â‚¬â„¢agit pas dÃ¢â‚¬â„¢une simple comparaison de chaÃƒÂ®nes de caractÃƒÂ¨res.
 * Veuillez vous fier Ãƒ  la spÃƒÂ©cification officielle (qui est trÃƒÂ¨s claire au sujet des noms) pour comparer les
 * noms. Vous nÃ¢â‚¬â„¢avez pas nÃƒÂ©cÃƒÂ©ssairement besoin de gÃƒÂ©rer tous les cas spÃƒÂ©ciaux, mais cela pourrait ÃƒÂªtre
 * utile dans dÃ¢â‚¬â„¢autres fonctions.
 * VÃƒÂ©rifie si un descripteur de fichier FAT identifie bien fichier avec le nom name
 * @param entry le descripteur de fichier
 * @param name le nom de fichier
 * @return 0 ou 1 (faux ou vrai)
 */
bool file_has_name(FAT_entry *entry, char *name) {
    int nbCharactere = 0;
    if (strcmp(name, "..") == 0)
        nbCharactere = 2;
    else if(strcmp(name, ".") == 0)
        nbCharactere = 1;
    else
        nbCharactere = 11;
    char *directory = malloc(nbCharactere+1);
    memset(directory, '\0', nbCharactere+1);
    memcpy(directory, entry->DIR_Name, nbCharactere);
    if (strncmp(directory, name, nbCharactere) == 0)
    {
        free(directory);
        return 1;
    }
    free(directory);
    return 0;
}

/**
 * Exercice 4
 * Cette fonction va vous servir Ãƒ  prendre un chemin dans lÃ¢â‚¬â„¢archive et le sÃƒÂ©parer en morceau. La spÃƒÂ©cification
 * exacte de la fonction est donnÃƒÂ©e en docstring au dessus de celle-ci. Cette fonction ne requiert
 * pas de connaissances particuliÃƒÂ¨res du systÃƒÂ¨me de fichier FAT32. Il faut bien sÃƒÂ»r allouer une chaÃƒÂ®ne
 * de caractÃƒÂ¨re pour y placer le morceau du chemin voulu. De plus, un indicateur de chemin relatif (ici
 * ".", "..") constitue un morceau de chemin valide, puisque ces marqueurs sont construits comme des
 * fichiers dans FAT32 (on laisse donc ces morceaux dans la chaÃƒÂ®ne). Le erreur code, en cas ou il nÃ¢â‚¬â„¢y a
 * pas dÃ¢â‚¬â„¢erreur, retourne la longueur de ce tableau.
 * Prend un path de la forme "/dossier/dossier2/fichier.ext et retourne la partie
 * correspondante Ãƒ  l'index passÃƒÂ©. Le premier '/' est facultatif.
 * @param path l'index passÃƒÂ©
 * @param level la partie Ãƒ  retourner (ici, 0 retournerait dossier)
 * @param output la sortie (la string)
 * @return -1 si les arguments sont incorrects, -2 si le path ne contient pas autant de niveaux
 * -3 si out of memory
 */
error_code break_up_path(char *path, uint8 level, char **output) {
    uint32 niveau = (uint32)level;
    if(path == NULL)
        return -1;
    char *strr;
    strr = malloc((strlen(path)+1) * sizeof(char));
    if(strr == NULL)
        return -3;
    strcpy(strr,path);
    char *token = strtok(strr, "/");
    uint32 indice = 0;
    while(token != NULL){
        if(indice == niveau){
            break;
        }
        token = strtok(NULL,"/");
        indice++;
    }
    if(indice != niveau){
        return -2;
    }
    char *motsemifinal = malloc((strlen(token)) * sizeof(char));
    if(motsemifinal == NULL)
        return OUT_OF_MEM;
    strncpy(motsemifinal,token,(strlen(token)));
    if (strcmp(motsemifinal, "..") == 0 || strcmp(motsemifinal, ".") == 0){
        *output = motsemifinal;
        return 0;
    }
    //char motfinal[12];
    char *motfinal = malloc(12 * sizeof(char));
    memset(motfinal, ' ', 12);
    char *tokenn = strtok(token, ".");
    if(tokenn){
        strncpy(motfinal, tokenn, strlen(tokenn));
        tokenn = strtok(NULL, ".");
        if(tokenn)
            strncpy((char *)(motfinal + 8), tokenn, strlen(tokenn));
        motfinal[11] = '\0';
        for (int i = 0; i < 11; i++)
        {
            motfinal[i] = toupper(motfinal[i]);
        }

    }
    else
    {
        strncpy(motfinal, motsemifinal, strlen(motsemifinal));
        motfinal[11] = '\0';
    }
    *output = motfinal;
    motfinal = NULL;
    free(motsemifinal);
    free(strr);
    return 0;
}

/**
 * Exercice 5
 * Cette fonction va lire le boot block dans lÃ¢â‚¬â„¢archive et va allouer la mÃƒÂ©moire nÃƒÂ©cÃƒÂ©ssaire pour le placer.
 * Cette fonction est essentielle pour le bon fonctionnement du reste du TP, puisque le boot block
 * contient toute lÃ¢â‚¬â„¢information nÃƒÂ©cÃƒÂ©ssaire pour la lecture de lÃ¢â‚¬â„¢archive. En particulier, le BPB donne des
 * informations quant Ãƒ  la gÃƒÂ©omÃƒÂ©trie du disque.
 * Lit le BIOS parameter block
 * @param archive fichier qui correspond Ãƒ  l'archive
 * @param block le block allouÃƒÂ©
 * @return un code d'erreur
 */
error_code read_boot_block(FILE *archive, BPB **block) {

    if (archive == NULL)
    {
        printf("Image does not exist\n");
        return GENERAL_ERR;
    }
    BPB *bpb = malloc(sizeof(BPB));
    if(bpb == NULL)
        return GENERAL_ERR;
    fread(bpb,90,1,archive);
    *block = bpb;
    clusterCourant = as_uint32(bpb->BPB_RootClus);
    return NO_ERR;
}
/**
 * Exercice 6
 * Cette fonction reÃƒÂ§oit en entrÃƒÂ©e un path qui correspond au chemin du fichier Ãƒ  aller chercher. Cette
 * fonction doit donc rechercher le fichier au travers lÃ¢â‚¬â„¢arborescence de dossiers. La fonction doit ÃƒÂªtre
 * capable de dÃƒÂ©terminer que le chemin nÃ¢â‚¬â„¢est pas valide. Par exemple, un chemin qui utilise un fichier
 * comme un dossier ne devrait pas fonctionner.
 * Trouve un descripteur de fichier dans l'archive
 * @param archive le descripteur de fichier qui correspond Ãƒ  l'archive
 * @param path le chemin du fichier
 * @param entry l'entrÃƒÂ©e trouvÃƒÂ©e
 * @return un code d'erreur
 */
error_code find_file_descriptor(FILE *archive, BPB *block, char *path, FAT_entry **entry) {
    if(path == NULL)
        return GENERAL_ERR;
    char *strr;
    strr = malloc((strlen(path)+1) * sizeof(char));
    if(strr == NULL)
        return GENERAL_ERR;
    strcpy(strr,path);
    char *token = strtok(strr, "/");
    uint32 nbbarre = 0;
    while(token != NULL) {
        token = strtok(NULL, "/");
        nbbarre++;
    }
    free(strr);
    uint16 reperoireCluster;
    FAT_entry repertoire[16];
    reperoireCluster = clusterCourant;
    char *nomDossier;
    bool foundtmp = 1;
    nbbarre--;
    uint32 offset =  ((reperoireCluster - 2) * as_uint16(block->BPB_BytsPerSec)) + (as_uint16(block->BPB_BytsPerSec) * as_uint16(block->BPB_RsvdSecCnt)) + (block->BPB_NumFATs * as_uint32(block->BPB_FATSz32) * as_uint16(block->BPB_BytsPerSec));
    fseek(archive, offset, SEEK_SET);
    fread(&repertoire,32,16,archive);
    for(int level = 0;level<nbbarre && foundtmp; level++){
        foundtmp = 0;
        //break_up_path(char *path, uint8 level, char **output)
        if(break_up_path(path,level,&nomDossier) >= 0){
            if(strlen(nomDossier) > 8 && nomDossier[9] != ' ' ){ /** un chemin qui utilise un fichier comme un dossier ne devrait pas fonctionner. */
                printf("Mauvaise chemin\n");
                return RES_NOT_FOUND;
            }
            for(int i = 0;i<16;i++){
                if(file_has_name(&repertoire[i], nomDossier)){
                    //printf("NomDossier: %s\n",repertoire[i].DIR_Name);
                    reperoireCluster = as_uint32(repertoire[i].DIR_FstClusLO);
                    uint32 offset =  ((reperoireCluster - 2) * as_uint16(block->BPB_BytsPerSec)) + (as_uint16(block->BPB_BytsPerSec) * as_uint16(block->BPB_RsvdSecCnt)) + (block->BPB_NumFATs * as_uint32(block->BPB_FATSz32) * as_uint16(block->BPB_BytsPerSec));
                    fseek(archive, offset, SEEK_SET);
                    fread(&repertoire,32,16,archive);
                    foundtmp = 1;
                    break;
                }

            }
            free(nomDossier);

        }else return GENERAL_ERR;
    }
    if(!foundtmp){
        return GENERAL_ERR;
    }
    int indice_fichier = -1;

    if(break_up_path(path,nbbarre,&nomDossier) >= 0){
        if(strlen(nomDossier) > 8 && nomDossier[9] == ' '){ /** un chemin qui utilise un fichier comme un dossier ne devrait pas fonctionner. */
            printf("Mauvaise chemin\n");
            return RES_NOT_FOUND;
        }
        for(int i = 0;i<16;i++){
            //printf("NomFichier: %s\n",repertoire[i].DIR_Name);
            if(&repertoire[i] !=NULL && file_has_name(&repertoire[i], nomDossier)){
                //printf("NomFichier: %s\n",repertoire[i].DIR_Name);
                indice_fichier  = i;
                break;
            }

        }
    }
    free(nomDossier);
    if(indice_fichier == -1){
        return GENERAL_ERR;
    }
    *entry =  malloc(sizeof(FAT_entry));
    memcpy(*entry,&repertoire[indice_fichier],32);


    return NO_ERR;
}

/**
 * Exercice 7
 *
 * Cette fonction prend en paramÃƒÂ¨tre une entrÃƒÂ©e FAT ainsi quÃ¢â‚¬â„¢un buffer et sa longueur maximale. Si
 * le descripteur dÃƒÂ©crit bien un fichier, le contenu du fichier est lu au complet (Ãƒ  moins de dÃƒÂ©passer la
 * longueur maximale, dans ce cas, il est partiellement lu) et placÃƒÂ© dans le buffer. Le code dÃ¢â‚¬â„¢erreur doit
 * retourner le nombre de bytes lu sÃ¢â‚¬â„¢il nÃ¢â‚¬â„¢y a pas dÃ¢â‚¬â„¢erreur. Si le fichier nÃ¢â‚¬â„¢est pas dans lÃ¢â‚¬â„¢arborescence, vous
 * devez retourner un code dÃ¢â‚¬â„¢erreur.
  *
 * Lit un fichier dans une archive FAT
 * @param entry l'entrÃƒÂ©e du fichier
 * @param buff le buffer ou ÃƒÂ©crire les donnÃƒÂ©es
 * @param max_len la longueur du buffer
 * @return un code d'erreur qui va contenir la longueur des donnÃƒÂ©s lues
 */
error_code read_file(FILE *archive, BPB *block, FAT_entry *entry, void *buff, size_t max_len) {
    uint16 fichierCluster = as_uint16(entry->DIR_FstClusLO);
    uint32 offset =  ((fichierCluster - 2) * as_uint16(block->BPB_BytsPerSec)) + (as_uint16(block->BPB_BytsPerSec) * as_uint16(block->BPB_RsvdSecCnt)) + (block->BPB_NumFATs * as_uint32(block->BPB_FATSz32) * as_uint16(block->BPB_BytsPerSec));
    fseek(archive, offset, SEEK_SET);
    char *buffer = (char*) buff;
    fread(buffer, max_len, 1, archive);
    buffer[max_len] = '\0';
    return 0;
}

int main(int argc, char *argv[]) {
    /** Open image file*/
    char file[] = "../floppy.img\0";
    FILE *imageFIle;
    FAT_entry *entree;
    char *chemin = "afolder/another/candide.txt";
    imageFIle = fopen(file, "r");
    if (imageFIle == NULL)
    {
        printf("%s Image does not exist\n",file);
        return -1;
    }
    printf("%s opened.\n", "floppy.img");
    BPB *block;
    read_boot_block(imageFIle, &block);
    if(HAS_NO_ERROR(find_file_descriptor(imageFIle, block, chemin, &entree))){
        size_t max_len = 20;
        char buff[max_len+1];
        read_file(imageFIle, block, entree,(void*)&buff, max_len);
        printf("%s\n",buff);
        free(entree);

    }else{
        printf("Fichier inexistant");
    }
    free(block);




/** Close image file*/
    fclose(imageFIle);
    imageFIle = NULL;
    return 0;
}