#define _XOPEN_SOURCE 700   /* So as to allow use of `fdopen` and `getline`.  */

#include "common.h"
#include "server_thread.h"

#include <netinet/in.h>
#include <netdb.h>

#include <strings.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/poll.h>
#include <sys/socket.h>

#include <time.h>

#include <errno.h>

#define NEEDED(i, j) (max_resources[i][j]-allocated_resources[i][j])

enum { NUL = '\0' };

enum {
  /* Configuration constants.  */
  max_wait_time = 30,
  server_backlog_size = 5
};

unsigned int server_socket_fd;

// Nombre de client enregistré.
int nb_registered_clients;
pthread_mutex_t nb_registered_clients_mutex = PTHREAD_MUTEX_INITIALIZER;


// Variable du journal.
// Nombre de requêtes acceptées immédiatement (ACK envoyé en réponse à REQ).
unsigned int count_accepted = 0;
pthread_mutex_t count_accepted_mutex = PTHREAD_MUTEX_INITIALIZER;



// Nombre de requêtes acceptées après un délai (ACK après REQ, mais retardé).
unsigned int count_wait = 0;
pthread_mutex_t count_wait_mutex = PTHREAD_MUTEX_INITIALIZER;



// Nombre de requête erronées (ERR envoyé en réponse à REQ).
unsigned int count_invalid = 0;
pthread_mutex_t count_invalid_mutex = PTHREAD_MUTEX_INITIALIZER;


// Nombre de clients qui se sont terminés correctement
// (ACK envoyé en réponse à CLO).
unsigned int count_dispatched = 0;
pthread_mutex_t count_dispatched_mutex = PTHREAD_MUTEX_INITIALIZER;


// Nombre total de requête (REQ) traités.
unsigned int request_processed = 0;
pthread_mutex_t request_dispatched_mutex = PTHREAD_MUTEX_INITIALIZER;


// Nombre de clients ayant envoyé le message CLO.
unsigned int clients_ended = 0;
pthread_mutex_t clients_ended_mutex = PTHREAD_MUTEX_INITIALIZER;


int number_clients = -1;
pthread_mutex_t number_clients_mutex = PTHREAD_MUTEX_INITIALIZER;

int total_number_resources = -1;
// TODO: Ajouter vos structures de données partagées, ici.
int * available_resources;
int ** max_resources;
int ** allocated_resources;

//bool server_started =false;

void handle_resource_init(cmd_header_t response_header, int argo[], int socket_fd);
void handle_begin(cmd_header_t request_header, int socket_fd);
void handle_request(cmd_header_t request_header, int argo[], int socket_fd);


// Inspire de https://www.geeksforgeeks.org/operating-system-bankers-algorithm/

bool is_in_safe_state(int client_id, int* resource_request)
{
  int *work=(int*)malloc(total_number_resources*sizeof(int));
  bool *finish=(bool*)malloc(number_clients*sizeof(bool));
  bool insufficient_resource_flag = false;
  int rank=0;

  for(int m=0;m<total_number_resources;++m)
  {
    work[m]=available_resources[m];
  }
  for(int n=0;n<number_clients;++n)
  {
    finish[n]=false;
  }

  for(int n=0;n<number_clients;++n)
  {
    if(finish[n] == false)
    {
      for(int m=0;m<total_number_resources;++m)
      {
        if(n!=client_id && NEEDED(n,m) > work[m])
        {
          insufficient_resource_flag = true;
        }
        else if(max_resources[n][m]-(allocated_resources[n][m]+resource_request[m]) > work[m])
        {
          insufficient_resource_flag =true;
        }
      }
      if(insufficient_resource_flag == false)
      {
        finish[n]=true;
        for(int m=0;m<total_number_resources;++m)
        {
          work[m] += allocated_resources[n][m];
        }
        ++rank;
        n=0;
      }
    }
  }
  free(work);
  free(finish);
  return rank == number_clients-1;
    
}

int should_allocate(int client_id, int* resource_request)
{
  for(int m=0;m<total_number_resources;++m)
  {
    if(NEEDED(client_id,m)< resource_request[m])
      {
        return -1;
      }
    if(resource_request[m]>available_resources[m])
    {
      return 0;
    }
  }
  return is_in_safe_state(client_id, resource_request);

}

void
st_init ()
{
  // Initialise le nombre de clients connecté.
  nb_registered_clients = 0;
  // TODO
  // Attend la connection d'un client et initialise les structures pour
  // l'algorithme du banquier.
  
  
  

//server_started=true;
  // END TODO
}

void
st_process_requests (server_thread * st, int socket_fd)
{
  // TODO: Remplacer le contenu de cette fonction
  printf("PROCESSING APPLICATION");

    struct cmd_header_t header = { .nb_args = 0 };

    //This is where we need to handle the signal
    //La premiere lecture pour recevoir le header
    int len = read_socket(socket_fd, &header, sizeof(header), max_wait_time * 1000);
    printf("ROCKS: %d - %d \n", len, errno);
    if (len > 0)
    {
				printf("Thread %d received command=%d, nb_args=%d\n", st->id, header.cmd, header.nb_args);
      	printf("header cmd recevied: %d \n",header.cmd);
      	printf("header num_args recevied: %d \n",header.nb_args);


					switch (header.cmd)
					{
						case BEGIN:
						{
							printf("BEG \n");
							int argo[header.nb_args];
							len = read_socket(socket_fd, &argo, sizeof(int)*header.nb_args, max_wait_time * 1000);
							for(int i=0;i<header.nb_args;i++)
							{
								printf("ARGS SENT TO BEG: %d - %d \n", i, *(argo+i));
							}
							number_clients = argo[1];
							handle_begin(header, socket_fd);
							break;
						}
						case CONF:
						{
							printf("CONF \n");
              int argo[header.nb_args];
              len = read_socket(socket_fd, &argo, sizeof(int)*header.nb_args, max_wait_time * 1000);
              for(int i=0;i<header.nb_args;i++)
              {
                      printf("ARGS SENT TO CONF: %d - %d \n", i, *(argo+i));
              }
              number_clients = argo[1];
							st_init();
							break;
						}
						case END:
						{
							end_server();
						}
						case INIT:
						{
							printf("INIT \n"); 
							int argo[header.nb_args];
							len = read_socket(socket_fd, &argo, sizeof(int)*header.nb_args, max_wait_time * 1000);
							for(int i=0;i<header.nb_args;i++)
							{
								printf("ARGS SENT TO INIT: %d - %d \n", i, *(argo+i));
							}
							handle_resource_init(header, argo, socket_fd);
							break;
						}
						case REQ:
						{
							printf("REQ \n");
							printf("How many to expect: %d \n", header.nb_args);
							int argo[header.nb_args];
							len = read_socket(socket_fd, &argo, sizeof(int)*header.nb_args, max_wait_time * 1000);
							//printf(argo);
							for(int i=0;i<header.nb_args;i++)
							{
                                                                //printf("Shoo: %d \n", ((argo+i)));
								//printf("3am: %d \n", (*(argo+i)));
								//printf("ARGS SENT TO REQ: %d - %d \n", i, *(argo+i));
                                                                printf("ARGS SENT TO REQ: %d - %d \n", i, argo[i]);

							}
							handle_request(header, argo, socket_fd);
							break;
						}
						case CLO:
						{
							int argo[header.nb_args];
							len = read_socket(socket_fd, &argo, sizeof(int)*header.nb_args, max_wait_time*1000);
							close_client(argo[0]);
						}

						default:
						{break;}
			}

    }
    else
    {
				if (len == 0)
        {
       	   fprintf(stderr, "Thread %d, connection timeout\n", st->id);
        }
    }

  //Donner des temps pour retourner les reponses
  usleep (random () % (100 * 1000));
  close(socket_fd);
}


void end_server()
{

}

void close_client(int client_id)
{

}

void handle_resource_init(cmd_header_t response_header, int argo[], int socket_fd)
{
	//Annonce le usage maximum du client
	//Le id du client est trouver dans argo[0]
}


void handle_request(cmd_header_t request_header, int argo[], int socket_fd)
{
	//TODO: Verifier si on peut allouer les resources
	//Le id du client est trouver dans argo[0]
  int client_id=argo[0];
  int* resource_request=malloc(total_number_resources*sizeof(int));
  for(int m=0;m<total_number_resources;++m)
  {
    resource_request[m]= argo[1+m];
  }
	int canGive = should_allocate(client_id,resource_request);
	if(canGive==-1)
  {
    struct cmd_header_t response_header;
    response_header.cmd=ERR;
  }
  
  if(canGive==0)
	{
		//On peut pas allouer - WAIT(5) 1 sec
		struct cmd_header_t response_header; 
		response_header.cmd=WAIT; 
		response_header.nb_args=1;
		int response[response_header.nb_args];
		
		time_t t;
		srand((unsigned) time(&t));
		response[0]=rand();
		
		
		send(socket_fd,response,response_header.nb_args*sizeof(int),0);
	}
	if(canGive==1)
	{
		//On peut allouer - Return ACK(4) 0
		struct cmd_header_t response_header; 
		response_header.cmd=ACK; 
		response_header.nb_args=0;
		send(socket_fd,&response_header,sizeof(response_header),0);
	}
}



void handle_begin(cmd_header_t request_header, int socket_fd)
{
		struct cmd_header_t response_header; 
		response_header.cmd=ACK; 
		response_header.nb_args=2;
		int response[response_header.nb_args];
		response[0]=1; // ACK correspond a commande #4
		response[1]=number_clients;
		printf("Check 0 \n");
		int back = send(socket_fd,&response_header,sizeof(response_header),0);
		printf("Check 1 \n");
		//usleep (random () % (100 * 1000));
		printf("LOL: %d - %d - %d", back, socket_fd, sizeof(response_header));
		if(back == sizeof(response_header))
		{
			send(socket_fd,response,sizeof(response),0);
		}

		printf("Sent begin response socket \n");

		//if(errorCaptureOne < 0 || errorCaptureTwo < 0)
		//{
		//	printf("Errors from begin");
		//}
}

void
st_signal ()
{
  // TODO: Remplacer le contenu de cette fonction

  // TODO end
}

void *
st_code (void *param)
{
  server_thread *st = (server_thread *) param;

  struct sockaddr_in thread_addr;
  socklen_t socket_len = sizeof (thread_addr);
  int thread_socket_fd = -1;
  int end_time = time (NULL) + max_wait_time;

  // Boucle jusqu'à ce que `accept` reçoive la première connection.
  while (thread_socket_fd < 0)
  {
  //Accept
    thread_socket_fd =
      accept (server_socket_fd, (struct sockaddr *) &thread_addr, &socket_len);

	//printf("server_socket_fd VALUE: %d \n", server_socket_fd);
	//printf("thread_socket_fd VALUE: %d - %d \n", thread_socket_fd, errno);

    if (time (NULL) >= end_time)
    {
      break;
    }
  }

  // Boucle de traitement des requêtes.
  while (accepting_connections)
  {
	//printf("Estoy acceptar los connections \n");
    if (!nb_registered_clients && time (NULL) >= end_time)
    {
      fprintf (stderr, "Time out on thread %d.\n", st->id);
      pthread_exit (NULL);
    }
    if (thread_socket_fd > 0)
    {
      st_process_requests (st, thread_socket_fd);
      close (thread_socket_fd);
      end_time = time (NULL) + max_wait_time;
    }
    thread_socket_fd =
      accept (server_socket_fd, (struct sockaddr *) &thread_addr,
          &socket_len);
  }

  return NULL;
}


//
// Ouvre un socket pour le serveur.
//
void
st_open_socket (int port_number)
{
  //Socket creation
  server_socket_fd = socket (AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
  if (server_socket_fd < 0)
    perror ("ERROR opening socket");

  //setsockopt
  if (setsockopt(server_socket_fd, SOL_SOCKET, SO_REUSEPORT, &(int){ 1 }, sizeof(int)) < 0) {
    perror("setsockopt()");
    exit(1);
  }

  struct sockaddr_in serv_addr;
  memset (&serv_addr, 0, sizeof (serv_addr));
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons (port_number);

  //Bind
  if (bind
      (server_socket_fd, (struct sockaddr *) &serv_addr,
       sizeof (serv_addr)) < 0)
    perror ("ERROR on binding");

  //Listen
  listen (server_socket_fd, server_backlog_size);
  //Accpeting socket connections happens in st_code
}


//
// Affiche les données recueillies lors de l'exécution du
// serveur.
// La branche else ne doit PAS être modifiée.
//
void
st_print_results (FILE * fd, bool verbose)
{
  if (fd == NULL) fd = stdout;
  if (verbose)
  {
    fprintf (fd, "\n---- Résultat du serveur ----\n");
    fprintf (fd, "Requêtes acceptées: %d\n", count_accepted);
    fprintf (fd, "Requêtes : %d\n", count_wait);
    fprintf (fd, "Requêtes invalides: %d\n", count_invalid);
    fprintf (fd, "Clients : %d\n", count_dispatched);
    fprintf (fd, "Requêtes traitées: %d\n", request_processed);
  }
  else
  {
    fprintf (fd, "%d %d %d %d %d\n", count_accepted, count_wait,
        count_invalid, count_dispatched, request_processed);
  }
}
