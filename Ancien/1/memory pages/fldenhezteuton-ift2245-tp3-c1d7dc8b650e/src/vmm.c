#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "conf.h"
#include "common.h"
#include "vmm.h"
#include "tlb.h"
#include "pt.h"
#include "pm.h"

static unsigned int read_count = 0;
static unsigned int write_count = 0;
static FILE* vmm_log;

//Done by François Luc Denhez-Teuton

static bool frame_table[NUM_FRAMES];
static bool is_filled = false;

void vmm_init (FILE *log)
{
  // Initialise le fichier de journal.
  vmm_log = log;
  for(int i=0;i<NUM_FRAMES;++i)
  {
    frame_table[i]=false;
  }
}


// NE PAS MODIFIER CETTE FONCTION
static void vmm_log_command (FILE *out, const char *command,
                             unsigned int laddress, /* Logical address. */
		             unsigned int page,
                             unsigned int frame,
                             unsigned int offset,
                             unsigned int paddress, /* Physical address.  */
		             char c) /* Caractère lu ou écrit.  */
{
  if (out)
    fprintf (out, "%s[%c]@%05d: p=%d, o=%d, f=%d pa=%d\n", command, c, laddress,
	     page, offset, frame, paddress);
}

/* Effectue une lecture à l'adresse logique `laddress`.  */
char vmm_read (unsigned int laddress)
{
  char c = '!';
  read_count++;

// DONE BY François Luc Denhez-Teuton
unsigned int page = laddress>>8;

unsigned int offset = laddress&255;

int frame = -1;

 if(tlb_verify(page)==true)
 {
   frame =tlb_lookup(page,false);
 }
 else
 {
   frame = pt_lookup(page);
   if(frame<0)
   {
     for(frame=0;frame<NUM_FRAMES && frame_table[frame];++frame){}
     if(frame==NUM_FRAMES)
     {
        is_filled=true;
        for(int i=0;i<NUM_FRAMES;++i)
        {
          frame_table[i]=false;
        }
        frame=0;
     }
      if(is_filled)
      {
        unsigned int page_to_replace = pt_lookdown((unsigned int)frame);
        pm_backup_page((unsigned int)frame,page_to_replace);
        pt_unset_entry(page_to_replace);
      }
      pm_download_page(page,(unsigned int)frame);
      pt_set_entry(page,(unsigned int)frame);
      tlb_add_entry(page,(unsigned int)frame,false); 
   }
   
 }
 
 unsigned int paddress=(((unsigned int)frame)<<8) + offset;
  c = pm_read(paddress);
  vmm_log_command (stdout, "READING", laddress, page, frame, offset, paddress, c);
  return c;
}

/* Effectue une écriture à l'adresse logique `laddress`.  */
void vmm_write (unsigned int laddress, char c)
{
  write_count++;

  unsigned int page = laddress>>8;

  unsigned int offset = laddress&255;

  int frame = -1;

 if(tlb_verify(page)==true)
 {
   frame =tlb_lookup(page,true);
   if(frame<0)
   {
     vmm_log_command (stdout, "WRITING PERMISSION DENIED", laddress, page, frame, offset, PHYSICAL_MEMORY_SIZE+1, c);
     return;
   }
 }
 else
 {
   frame = pt_lookup(page);
   if(frame<0)
   {
     for(frame=0;frame<NUM_FRAMES && frame_table[frame];++frame){}
     if(frame==NUM_FRAMES)
     {
         for(int i=0;i<NUM_FRAMES;++i)
          {
            frame_table[i]=false;
          }
          frame=0;
     }
     if(is_filled)
      {
         unsigned int page_to_replace = pt_lookdown((unsigned int)frame);
        pm_backup_page((unsigned int)frame,page_to_replace);
        pt_unset_entry(page_to_replace);
      }
      pm_download_page(page,(unsigned int)frame);
      pt_set_entry(page,(unsigned int)frame);
      tlb_add_entry(page,(unsigned int)frame,pt_readonly_p(page)); 
   }
 }
 
 unsigned int paddress=(((unsigned int)frame)<<8) + offset;
  pm_write(paddress,c);
  
  vmm_log_command (stdout, "WRITING", laddress, page, frame, offset, paddress, c);
}


// NE PAS MODIFIER CETTE FONCTION
void vmm_clean (void)
{
  fprintf (stdout, "VM reads : %4u\n", read_count);
  fprintf (stdout, "VM writes: %4u\n", write_count);
}
