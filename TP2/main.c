/**
Lenny SIEMENI 1055234
Mehran ASADI 1047847
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <memory.h>
#include <pthread.h>
#include <unistd.h>
#include <wait.h>
#include <sys/types.h>


typedef unsigned char bool;
static const bool False = 0;
static const bool True = 1;
typedef struct command_struct command;
typedef struct command_chain_head_struct command_head;
int *ressource_cap_max;
typedef enum {
    BIDON, NONE, OR, AND, ALSO
} operator;

struct command_struct {
    int *ressources;
    char **call;
    int call_size;
    int count;
    operator op;
    command *next;
};

struct command_chain_head_struct {
    int *max_resources;
    int max_resources_count;
    int max_command;
    command *command;
    pthread_mutex_t mutex;
    bool background;
};

// Forward declaration
typedef struct banker_customer_struct banker_customer;
typedef enum {
    NEW,READY,RUNNING,WAITING,BANKER
} etat_enum;
struct banker_customer_struct {
    command_head *head;
    banker_customer *next;
    banker_customer *prev;
    etat_enum etat;
    int *current_resources;
    int depth;
};

typedef int error_code;
#define HAS_ERROR(err) ((err) < 0)
#define HAS_NO_ERROR(err) ((err) >= 0)
#define NO_ERROR 0
#define ERROR (-1)
#define NULL_TERMINATOR '\0'
#define FS_CMDS_COUNT 10
#define FS_CMD_TYPE 0
#define NETWORK_CMDS_COUNT 6
#define NET_CMD_TYPE 1
#define SYS_CMD_COUNTS 3
#define SYS_CMD_TYPE 2

typedef struct {
    char **commands;
    int *command_caps;
    unsigned int command_count;
    unsigned int ressources_count;
    int file_system_cap;
    int network_cap;
    int system_cap;
    int any_cap;
    int no_banquers;
} configuration;

// ---------------------------------------------------------------------------------------------------------------------
error_code readLine(char **out) {
    size_t size = 10;                       // size of the char array
    char *line = malloc((size + 1) * sizeof(char) );       // initialize a ten-char line
    if (line == NULL) return ERROR;   // if we can't, terminate because of a memory issue
    int at = 0;
    char *tempPtr = NULL;
    while (1) {
        if (at >= size) {

            tempPtr =  realloc(line,  size*2);
            if (tempPtr == NULL) {
                free(line);
                return ERROR;
            }
            size = 2 * size;
//            for(int ii =at;ii<size;ii++){
//                line[ii] = NULL_TERMINATOR;
//            }
            line = tempPtr;
            tempPtr = NULL;
        }

        int tempCh = getchar();


        if (tempCh == EOF) { // if the next char is EOF, terminate
            free(line);
            return ERROR;
        }

        char ch = (char) tempCh;
        /** Si il y a multiple space ensemble, alors le transformer en 1 seul.**/
        if(ch == ' '){
            line[at] = ch; // sets ch at the current index and increments the index
            at++;
            if (at >= size) {
                size = 2 * size;
                char *tempPtr = realloc(line, (size + 1) * sizeof(char));
                if (tempPtr == NULL) {
                    free(line);
                    return ERROR;
                }


                line = tempPtr;
                tempPtr = NULL;
            }
            tempCh = getchar();
            while(tempCh == ' ')
                tempCh = getchar();
            if (tempCh == EOF) { // if the next char is EOF, terminate
                free(line);
                return ERROR;
            }
            ch = (char) tempCh;
        }

        if (ch == '\n') {        // if we get a newline
            line[at] = NULL_TERMINATOR;    // finish the line with return 0
            if(line[at-1] == ' '){
                line[at-1] = NULL_TERMINATOR;
                at--;
            }


            //printf("line[at-2]: %c\n",line[at-2]);
            break;
        }



        line[at] = ch; // sets ch at the current index and increments the index
        at++;
    }

    out[0] = line;
    return 0;
}
void freeStringArray(char **arrs) {
    char **arr = arrs;
    if (arr != NULL) {
        free(arr[0]);
    }
    free(arr);
}

command *freeAndNext(command *current) {
    if (current == NULL) return current;

    command *next = current->next;
    if(current->call != NULL) {
        freeStringArray(current->call);
    }
    if(current->ressources != NULL)
        free(current->ressources);
    free(current);
    return next;
}

void freeCommands(command *head) {
    command *current = head;
    while (current != NULL) current = freeAndNext(current);
}
void freeheader(command_head *head) {
    if(head != NULL){

        if(head->command != NULL)
            freeCommands(head->command);
        if(head->max_resources != NULL)
            free(head->max_resources);
        free(head);
    }
}

// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

// Configuration globale
configuration *conf = NULL;

void freeconfig(){
    for(int i = 0;i<conf->command_count;i++){
        free(conf->commands[i]);
    }
    free(conf->command_caps);
    free(conf);
}

/**
 * Cette fonction analyse la première ligne et remplie la configuration
 * @param line la première ligne du shell
 * @param conf pointeur vers le pointeur de la configuration
 * @return un code d'erreur (ou rien si correct)
 */
error_code parse_first_line(char *res_name) {
    char *strr;
    strr = malloc((strlen(res_name)+1) * sizeof(char));
    if(strr == NULL)
        return 1;
    strcpy(strr,res_name);
    char **temp = malloc((6) * sizeof(char *));
    if(temp ==NULL)
        return 1;
    //printf("line: %s\n",strr);
    int copy_index = 0;
    char *token = strtok(strr, "&");
    char *arg;
    while (token != NULL) {
        arg = malloc(sizeof(char) * (strlen(token)+1));
        /* if (NULL == (arg )) {
             goto error;
         }*/

        strcpy(arg, token);
        temp[copy_index] = arg;

        token = strtok(NULL, "&");

        copy_index++;
    }
    int *table_ressource_Max = malloc(copy_index*sizeof(int*));
    if(table_ressource_Max == NULL)
        return ERROR;
    unsigned int command_count = 0;
    char **commands_tmp = NULL;
    int *command_caps = NULL;
    if(copy_index < 5){
        table_ressource_Max[FS_CMD_TYPE] = atoi(temp[0]);
        table_ressource_Max[NET_CMD_TYPE] = atoi(temp[1]);
        table_ressource_Max[SYS_CMD_TYPE] = atoi(temp[2]);
        table_ressource_Max[copy_index-1] = atoi(temp[3]);
    }
    if(copy_index > 4){
        table_ressource_Max[FS_CMD_TYPE] = atoi(temp[2]);
        table_ressource_Max[NET_CMD_TYPE] = atoi(temp[3]);
        table_ressource_Max[SYS_CMD_TYPE] = atoi(temp[4]);
        table_ressource_Max[copy_index-1] = atoi(temp[5]);
        int nbVirgul = 0;
        int comcap_len = strlen(temp[1]);
        for(int i =0;i<comcap_len;i++){
            if(temp[1][i]==',')
                nbVirgul++;
        }
        command_count = nbVirgul + 1;
        commands_tmp = malloc((command_count+1) * sizeof(char*));
        command_caps = malloc((command_count) * sizeof(int));


        char *tokenCom = strtok(temp[0], ",");
        int copy_indice = 0;
        while (tokenCom != NULL) {
            char *arg = malloc(sizeof(char) * (strlen(tokenCom)+1));

            strcpy(arg, tokenCom);
            commands_tmp[copy_indice] = arg;

            tokenCom = strtok(NULL, ",");
            copy_indice++;
        }
        commands_tmp[copy_indice] = NULL;

        char *tokenCap = strtok(temp[1], ",");
        copy_indice = 0;
        while (tokenCap != NULL) {
            table_ressource_Max[SYS_CMD_TYPE+1+copy_indice] = atoi(tokenCap);
            command_caps[copy_indice] = table_ressource_Max[SYS_CMD_TYPE+1+copy_indice];
            tokenCap = strtok(NULL, ",");
            copy_indice++;
        }

    }
    conf = malloc(sizeof(configuration));
    if(conf == NULL)
        return ERROR;

    conf->file_system_cap = table_ressource_Max[FS_CMD_TYPE];
    conf->network_cap = table_ressource_Max[NET_CMD_TYPE];
    conf->system_cap = table_ressource_Max[SYS_CMD_TYPE];
    conf->any_cap = table_ressource_Max[copy_index-1];

    conf->command_count = command_count;
    conf->commands = commands_tmp;
    conf->command_caps = command_caps;
    conf->ressources_count = copy_index;
    ressource_cap_max = malloc((copy_index)*sizeof(int*));
    for(int i =0;i<copy_index;i++){
        ressource_cap_max[i] = table_ressource_Max[i];
    }
    free(table_ressource_Max);
    for(int i = 0;i<6;i++){
        free(temp[i]);
    }
    free(strr);
    free(temp);
    return NO_ERROR;
}


const char *FILE_SYSTEM_CMDS[FS_CMDS_COUNT] = {
        "ls",
        "cat",
        "find",
        "grep",
        "tail",
        "head",
        "mkdir",
        "touch",
        "rm",
        "whereis"
};

const char *NETWORK_CMDS[NETWORK_CMDS_COUNT] = {
        "ping",
        "netstat",
        "wget",
        "curl",
        "dnsmasq",
        "route"
};

const char *SYSTEM_CMDS[SYS_CMD_COUNTS] = {
        "uname",
        "whoami",
        "exec"
};

/**
 * Cette fonction prend en paramètre un nom de ressource et retourne
 * le numéro de la catégorie de ressource
 * @param res_name le nom
 * @param config la configuration du shell
 * @return le numéro de la catégorie (ou une erreur)
 */
error_code resource_no(char *res_name) {
    int nbCommandName = conf->command_count;
    char *nom_res = malloc((strlen(res_name)+1) * sizeof(char*));
    strcpy(nom_res,res_name);
    int num_ressource = ERROR;
    for(int i=0;i<nbCommandName;i++){
        //printf("conf->commands[%d]: %s\n",i,conf->commands[i]);
        if(!strcmp(nom_res,conf->commands[i])){
            //printf("Trouverconf->commands[%d]: %s\n",i,conf->commands[i]);
            num_ressource = i;
        }
    }
    if(num_ressource>=0){
        num_ressource = SYS_CMD_TYPE + 1 + num_ressource;
    }
    else{
        for(int i=0;i<FS_CMDS_COUNT;i++){
            if(!strcmp(nom_res,FILE_SYSTEM_CMDS[i])){
                num_ressource = FS_CMD_TYPE;
            }
        }
        if(num_ressource < 0){
            for(int i=0;i<NETWORK_CMDS_COUNT;i++){
                if(!strcmp(nom_res,NETWORK_CMDS[i])){
                    num_ressource = NET_CMD_TYPE;
                }
            }
            if(num_ressource < 0){
                for(int i=0;i<SYS_CMD_COUNTS;i++){
                    if(!strcmp(nom_res,SYSTEM_CMDS[i])){
                        num_ressource = SYS_CMD_TYPE;
                    }
                }
                if(num_ressource < 0){
                    num_ressource = conf->ressources_count - 1;
                }
            }
        }
    }
    free(nom_res);
    return num_ressource;
}


/**
 * Cette fonction prend en paramètre un numéro de ressource et retourne
 * la quantitée disponible de cette ressource
 * @param resource_no le numéro de ressource
 * @param conf la configuration du shell
 * @return la quantité de la ressource disponible
 */
int resource_count(int resource_no) {

    return ressource_cap_max[resource_no];
}

// Forward declaration
error_code evaluate_whole_chain(command_head *head);

/**
 * Créer une chaîne de commande qui correspond à une ligne de commandes
 * @param config la configuration
 * @param line la ligne de texte à parser
 * @param result le résultat de la chaîne de commande
 * @return un code d'erreur
 */
error_code create_command_chain(const char *line, command_head **result) {
    command_head *debut = NULL;
    debut = malloc(sizeof(command_head));
    if(debut == NULL)
        return ERROR;
    debut->max_resources = NULL;
    debut->background = False;
    debut->command = NULL;
//    debut->mutex = NULL;
    debut->max_command = 0;
    char **call = NULL;
    char *wordPtr = NULL;
    int currentCallSize = 0;

    command *c = NULL;
    command *head = NULL;
    command *current = NULL;
    int escaped = 0;
    int parentOuvert = 0;
    for (int index = 0; line[index] != NULL_TERMINATOR; index++) {
        parentOuvert = 0;
        int nextSpace = index;
        while (1) {
            if (!parentOuvert) { // if(escaped == 0 && (line[nextSpace] == ' ' || line[nextSpace] == '/0'))
                //printf("index = %d    nextSpace = %d,  line[nextSpace]  = %c\n",index,nextSpace,line[nextSpace]);
                if (line[nextSpace] == ' ' || line[nextSpace] == NULL_TERMINATOR ) {
                    //printf("break index = %d    nextSpace = %d,  line[nextSpace]  = %c\n",index,nextSpace,line[nextSpace]);
                    break;
                }
            }
            if(line[nextSpace] == NULL_TERMINATOR){
                break;
            }
            if (line[nextSpace] == '('){ parentOuvert++;}
            if (line[nextSpace] == ')'){ parentOuvert--;}
            nextSpace++;
        }
        //printf("Sortie index = %d    nextSpace = %d,  line[nextSpace]  = %c\n",index,nextSpace,line[nextSpace]);
        wordPtr = malloc(sizeof(char) * (nextSpace - index + 1)); /**  Allouer espace pour le mot **/
        if (wordPtr == NULL) goto error;

        int i = 0;
        for (; i < nextSpace - index; i++) wordPtr[i] = line[i + index]; /**  wordPtr = line[index  a nextSpace] **/
        wordPtr[i] = NULL_TERMINATOR;

        operator op = BIDON;
        if (strcmp(wordPtr, "||") == 0) op = OR;
        if (strcmp(wordPtr, "&&") == 0) op = AND;
        if (strcmp(wordPtr, "&") == 0) op = ALSO;
        if (op != ALSO && line[nextSpace] == NULL_TERMINATOR)op = NONE;

        if(op == OR || op == AND || op == ALSO) free(wordPtr);

        if (op == BIDON || op == NONE) {
            if (call == NULL) {
                currentCallSize = 1;
                call = malloc((currentCallSize + 1)*sizeof(char *));
                if (call == NULL) goto error;
                call[1] = NULL;
            } else {
                currentCallSize++;
                char **tempPtr = realloc(call, (currentCallSize + 1) * sizeof(char *));
                if (tempPtr == NULL) goto error;

                call = tempPtr;
                call[currentCallSize] = NULL;
            }
            //printf("currentCallSize = %d\n",currentCallSize);
            call[currentCallSize - 1] = wordPtr;
        }

        if (op != BIDON) {
            if (call == NULL) goto errorOne;
            c = malloc(sizeof(command));
            if (c == NULL) goto error;

            if (head == NULL){
                head = c;
                debut->command = head;
            }
            else current->next = c;

            c->count = 1;
            c->ressources = NULL;
            c->call = NULL;
            /** -----------------------------------rn et fn--------------------------------------------------------------------**/
            if (call[0][strlen(call[0]) - 1] == ')' && (call[0][0] == 'r'|| call[0][0] == 'f')) {
                char *command = call[0];  /** command = call[0]  **/

                unsigned long command_len = strlen(command); //rn et fn ici

                int paren_pos = 0;
                for (; command[paren_pos] != '('; paren_pos++); /** paren_pos++ jusquau charactere (   **/
                //printf("command_len: %d,  paren_pos: %d, command[paren_pos]: %c,  command: %s\n",command_len,paren_pos,command[paren_pos],command);
                wordPtr = malloc(paren_pos * sizeof(char));  /**  on affectera a wordPtr la valeur litteral N   **/
                if(wordPtr == NULL) goto error;
                //isdigit(c)
                int goodSyntaxe = 1;
                for (int j = 0; j < paren_pos; j++){
                    if(isdigit(command[j + 1]))
                        wordPtr[j] = command[j + 1];
                    else
                    if(j + 1 != paren_pos){
                        goodSyntaxe = 0;
                        free(wordPtr);
                        command = NULL;
                        break;
                    }

                }
                if(goodSyntaxe){
                    //printf("wordPtr:%s  ,wordPtr[paren_pos - 1]: %c\n",wordPtr,wordPtr[paren_pos - 1]);
                    wordPtr[paren_pos - 1] = NULL_TERMINATOR; /**  mettre a '/0' le charactere '('  **/

                    int nb = atoi(wordPtr); /**  nb = int(N) **/ // NOLINT(cert-err34-c)
                    c->count = ('r' == command[0]) ? nb : -nb; /**  si rN alors c.count = nb, sinon rN = -nb **/

                    free(wordPtr);
                    if (NULL == (wordPtr = malloc((command_len - paren_pos - 1) * sizeof(char)))) {
                        goto error;
                    }

                    memcpy(wordPtr, &command[paren_pos + 1], command_len - paren_pos - 2); /** wordPtr = tout les mots entre parenthese excluant les parentheses  **/
                    //printf("wordPtr:%s  ,wordPtr[command_len - paren_pos - 3]: %c\n",wordPtr,wordPtr[command_len - paren_pos - 3]);
                    wordPtr[command_len - paren_pos - 2] = NULL_TERMINATOR;
                    if(wordPtr[command_len - paren_pos - 3] == ' ')
                        wordPtr[command_len - paren_pos - 3] = NULL_TERMINATOR;
                    free(command);

                    unsigned long resized_len = strlen(wordPtr);

                    int space_nb = 0;
                    for (int j = 0; j < resized_len; j++) {
                        space_nb += wordPtr[j] == ' ';
                    }

                    char **temp;
                    if (NULL == (temp = realloc(call,(space_nb + 2) * sizeof(char *)))) {
                        goto error;
                    } else {
                        call = temp;
                        call[space_nb + 1] = NULL;
                    }
                    int copy_index = 0;
                    char *token = strtok(wordPtr, " ");
                    while (token != NULL) {
                        char *arg = malloc(sizeof(char) * (strlen(token)+1));
                        /* if (NULL == (arg )) {
                             goto error;
                         }*/

                        strcpy(arg, token);
                        call[copy_index] = arg;

                        token = strtok(NULL, " ");

                        copy_index++;
                    }

                    free(wordPtr);
                }}
/** -------------------------------------------------------------------------------------------------------**/
            c->call = call;
            c->call_size = currentCallSize;
            c->next = NULL;
            c->op = op;
            if (op == ALSO) {
                c->op = NONE;
                debut->background = True;
            }

            current = c;

            call = NULL;

        }

        if (line[nextSpace] == NULL_TERMINATOR) break;
        index = nextSpace;
    }
    error_code code_error = evaluate_whole_chain(debut);
    if(code_error == -6) goto errorContinue;
    if(HAS_ERROR(code_error)) goto error;
    result[0] = debut;
    return 0;

    errorOne:
    free(wordPtr);
    freeCommands(head);
    return ERROR;
    error:
    free(wordPtr);
    //free(line);
    freeStringArray(call);
    freeAndNext(c);
    freeheader(debut);
    return ERROR;
    errorContinue:
    free(wordPtr);
    //free(line);
    freeStringArray(call);
    freeAndNext(c);
    //freeheader(debut);
    return -6;
}

/**
 * Cette fonction compte les ressources utilisées par un block
 * La valeur interne du block est mise à jour
 * @param command_block le block de commande
 * @return un code d'erreur
 */
error_code count_ressources(command_head *head, command *command_block) {
    int nbRessource = head->max_resources_count;
    int *table_ressource = malloc(nbRessource*sizeof(int));
    if(table_ressource == NULL)
        return ERROR;
    for(int i = 0;i<nbRessource;i++){
        table_ressource[i] = 0;
    }
    command_block->ressources = table_ressource;
    int ressource_num = resource_no(command_block->call[0]);

    command_block->ressources[ressource_num] = command_block->count;
    head->max_resources[ressource_num] += command_block->count;
    if(head->max_resources[ressource_num] < 0 )
        head->max_resources[ressource_num] = 0;
    head->max_command++;

    return NO_ERROR;
}

/**
 * Cette fonction parcours la chaîne et met à jour la liste des commandes
 * @param head la tête de la chaîne
 * @return un code d'erreur
 */
error_code evaluate_whole_chain(command_head *head) {
    /** Initialiser la table_ressource pour head**/
    int nbRessource = conf->ressources_count;
    int *table_ressource = malloc(nbRessource*sizeof(int));
    if(table_ressource == NULL)
        return ERROR;
    for(int i = 0;i<nbRessource;i++){
        table_ressource[i] = 0;
    }
    head->max_resources = table_ressource;
    head->max_resources_count = nbRessource;
    command *current = NULL;
    current = head->command;
    while(current != NULL){
        error_code code_error = count_ressources(head,current);
        if(code_error < 0)
            return code_error;
        current = current->next;
    }
    for(int i = 0;i<nbRessource;i++){
        if(head->max_resources[i] > resource_count(i)){
            printf("Ressource demandee depasse le maximum\n");
            return -6;
        }
    }


    return NO_ERROR;
}

// ---------------------------------------------------------------------------------------------------------------------
//                                              BANKER'S FUNCTIONS
// ---------------------------------------------------------------------------------------------------------------------
int thread_principal;
int *_available = NULL;
size_t nbRessourrce;
static banker_customer *first;
static pthread_mutex_t register_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t reading_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t available_mutex = PTHREAD_MUTEX_INITIALIZER;


error_code freeBanker (banker_customer *customer){
    if(customer == NULL)
        ERROR;
    banker_customer *current = customer;
    freeheader(current->head);
    free(current->current_resources);
    current = NULL;
    NO_ERROR;
}
void freeAndNextBanker(){
    banker_customer *current = first;
    banker_customer *next;
    if(HAS_NO_ERROR(freeBanker(current))){
        next = current->next;
        next->prev = NULL;
        current->next = NULL;
        free(current);
        current = next;
    }

}

error_code request_resource(banker_customer *customer, int cmd_depth);

/**
 * Cette fonction enregistre une chaîne de commande pour être exécutée
 * Elle retourne NULL si la chaîne de commande est déjà enregistrée
 *                       ou une erreur se produit pendant l'exécution.
 * TODO Comme plusieurs threads de client peuvent appeler cette fonction,
 *   afin de garder la cohérence dans le tableau on doit utiliser mécanismes de synchronisation nécessaires et suffisants.
 * @param head la tête de la chaîne de commande
 * @return le pointeur vers le compte client retourné
 */
banker_customer *register_command(command_head *head) {
    banker_customer *bankerr = malloc(sizeof(banker_customer));
    if(bankerr == NULL) return NULL;
    int ressCount = head->max_resources_count;

    bankerr->current_resources = malloc(ressCount*sizeof(int));
    if( bankerr->current_resources == NULL) return NULL;
    for(int i =0;i<ressCount;i++)  bankerr->current_resources[i] = 0;
    bankerr->head = head;
    bankerr->next = NULL;
    bankerr->prev = NULL;
    bankerr->depth = -1;
    bankerr->etat = NEW;
    pthread_mutex_lock(&register_mutex); //Lock the first
    /** Effectuer les operations**/
    banker_customer *current = first;
    if(current == NULL)
        first = bankerr;
    else{
        //banker_customer *suiv = debut->next;
        while(current->next != NULL){
            if(current->head == head){  /** retourner NULL si la chaîne de commande est déjà enregistrée**/
                pthread_mutex_unlock(&register_mutex);
                return NULL;
            }
            current = current->next;
        }
        if(current->head == head){ /** retourner NULL si la chaîne de commande est déjà enregistrée**/
            pthread_mutex_unlock(&register_mutex);
            return NULL;
        }
        current->next = bankerr;
        bankerr->prev = current;
    }
    pthread_mutex_unlock(&register_mutex); //Unlock the first

    return bankerr;
}

/**
 * Cette fonction enlève un client de la liste de client de l'algorithme du banquier.
 * Elle libère les ressources associées au client.
 * @param customer le client à enlever
 * @return un code d'erreur
 */
error_code unregister_command(banker_customer *customer) {
    /** Effectuer les operations**/
    if(customer != NULL){
        pthread_mutex_lock(&available_mutex);
        //Libèrer les ressources associées au client
        //printf("Liberer espace\n");
        for(int i =0;i<customer->head->max_resources_count;i++){
            _available[i] += customer->current_resources[i];
        }
        pthread_mutex_unlock(&available_mutex);
        pthread_mutex_lock(&register_mutex);
        if(customer->prev != NULL){
            customer->prev->next = customer->next;
        }else{
            first = customer->next;
        }
        if(customer->next != NULL){
            customer->next->prev = customer->prev;
        }
        customer->next = NULL;
        customer->prev = NULL;
        pthread_mutex_unlock(&register_mutex);
        int isBackg = customer->head->background;
        freeBanker(customer);
        free(customer);
        if(!isBackg)
            pthread_mutex_unlock(&reading_mutex);
        if((int)pthread_self()!=thread_principal){
            //printf("pthread_exit\n");
            pthread_exit(pthread_self());
        }

        return NO_ERROR;
    }
    return ERROR;
}

/**
 * Exécute l'algo du banquier sur work et finish.
 *
 * @param work
 * @param finish
 * @return
 */
int bankers(int *work, int *finish,int nbComm) {
    /**
     * Retourner
        faux si la requête décrite par work et finish n’est pas valide/sécuritaire.
        vrai sinon.
     */
    for(int j=0;j<nbRessourrce;j++){
        if(work[j] < 0)
            return False;
    }
    for(int i=0;i<nbComm;i++){
        if(finish[i] == 0)
            return False;
    }
    return True;
}
void *wake_neighbor(banker_customer *customer);

int callCommand(command *current) {
    if (current->count <= 0) return 1;

    int exitCode = -1;     // the exit code of the child
    //char *cmd_name = current->call[0];
    //char **cmd_all = current->call;
    pid_t pid = 0;
    for (int i = 0; i < current->count; i++) {
        pid = fork();

        if (pid < 0) {        // forking failed
            return pid;
        } else if (pid == 0) {
            // -----------------------------------------------------
            //                    CHILD PROCESS
            // -----------------------------------------------------
            char *cmd_name = current->call[0];
            execvp(cmd_name, current->call);    // execvp searches for command[0] in PATH, and then calls command

            printf("bash: %s: command not found\n", cmd_name);    // if we reach this, exec couldn't find the command

            exit(-1);    // and then we exit with 2, signaling an error. This also frees ressources
        }
    }

    // PID is correct
    waitpid(pid, &exitCode, 0); // Wait for the child process to exit.
    int x = WIFEXITED(exitCode);
    if (!x) return 0;

    x = WEXITSTATUS(exitCode);
    if (x != 0) return 0;
    return 1;
}

error_code callCommands(banker_customer *customer) {
    command *current = customer->head->command;
    int depth = customer->depth;
    for(int i =0;i<depth;i++) current = current->next;
    if (current == NULL || current->call == NULL) return 0;

    int ret = callCommand(current);
    if (ret == -1) return ERROR;

    operator op = current->op;
    command *next = current->next;
    depth++;
    switch (op) {
        default:
        case NONE:
        case BIDON:
            unregister_command(customer);
            return 0;
        case AND:
            if (ret) request_resource(customer, depth);
            return 0;
        case OR:
            if (ret) {
                depth++;
                next = next->next;
                while (next != NULL && (next->op == OR || next->op == NONE)){depth++; next = next->next;}
                if (next != NULL && next->op == AND){depth++; next = next->next;}
                if(next != NULL) request_resource(customer, depth);
                return 0;
            }
            else
                request_resource(customer, depth);

            return 0;
    }
}
/**
 * Prépare l'algo. du banquier.
 *
 * Doit utiliser des mutex pour se synchroniser. Doit allouer des structures en mémoire. Doit finalement faire "comme si"
 * la requête avait passé, pour défaire l'allocation au besoin...
 *
 * @param customer
 */
void call_bankers(banker_customer *customer) {
    pthread_mutex_lock(&available_mutex);
    //printf("call_bankers with id: %d\n",pthread_self());
    int nbRessource = customer->head->max_resources_count;
    int depth = customer->depth;
    command *current_command = customer->head->command;
    /**Tester les conditions de deadlock évidentes (qui n’ont pas besoin de l’algorithme du banquier)*/
    for(int i =0;i<depth;i++) current_command = current_command->next;
    //bool isFn = False;
    for(int j=0;j<nbRessource;j++){
        if(_available[j] < current_command->ressources[j]){
            pthread_mutex_unlock(&available_mutex);
            customer->etat = WAITING;
            wake_neighbor(customer);
            return;
        }
        if(current_command->ressources[j] < 0){
            //isFn = True;
            if(customer->current_resources[j] == 0){
                pthread_mutex_unlock(&available_mutex);
                customer->etat = RUNNING;
                callCommands(customer);
                return;
            }
            int fn_pos = current_command->ressources[j] * (-1);
            if(customer->current_resources[j] >= fn_pos){
                _available[j] -= current_command->ressources[j];
                customer->current_resources[j] += current_command->ressources[j];
            }else{
                _available[j] -= customer->current_resources[j];
                customer->current_resources[j] = 0;
            }
            pthread_mutex_unlock(&available_mutex);
            customer->etat = RUNNING;
            callCommands(customer);
            return;
        }
    }
    for(int j=0;j<nbRessource;j++){
        _available[j] -= current_command->ressources[j];
        customer->current_resources[j] += current_command->ressources[j];
    }

    int nbCommand = customer->head->max_command;
    /**Allouer les tableaux pertinents (work et finish)*/
    int *work = malloc(nbRessource*sizeof(int));
    if(work==NULL) return;
    int *finish = malloc(nbCommand*sizeof(int*));
    if(finish==NULL)return;
    for(int i=0;i<=depth;i++){
        finish[i] = 1;
    }
    int tmpdepth = depth+1;
    for(int i=tmpdepth;i<nbCommand;i++){
        finish[i] = 0;
    }
    /**Changer provisoirement available (le tableau des ressources qu’on a en banque) comme si la requête à la profondeur customer->depth avait passé*/
    for(int j=0;j<nbRessource;j++){
        work[j] = _available[j];
    }
    command *tmp_command = current_command;
    for(int i=depth;i<nbCommand;i++){

        if(!finish[i]){
            int j = 0;
            while(j < nbRessource){
                if(work[j] < tmp_command->ressources[j])
                    break;
                j++;
            }
            if(j == nbRessource){
                for(int jj=0;jj<nbRessource;jj++)
                    work[jj] -= tmp_command->ressources[jj];
                finish[i] = 1;
            }
        }
        tmp_command = tmp_command->next;
    }

    /**
     * TODO Libérer (OU PAS) les clients qui attendent sur leur propre mutex
     *  (tel que décrit dans request_ressources) selon la valeur de retour de bankers.
     */
    int bankretour = bankers(work,finish,nbCommand);
    free(finish);
    free(work);
    if(bankretour == False){
        //printf("bankers Refuse with id: %d\n",pthread_self());
        /** Défaire les changements provisoires qu’on  à  fait  à  available.*/
        for(int j=0;j<nbRessource;j++){
            _available[j] += current_command->ressources[j];
            customer->current_resources[j] -= current_command->ressources[j];
        }
        pthread_mutex_unlock(&available_mutex);
        customer->etat = WAITING;
        wake_neighbor(customer);
    }
    else{
        pthread_mutex_unlock(&available_mutex);
        /**Call current_command*/
        customer->etat = RUNNING;
        //pthread_mutex_t tmp = customer->head->mutex;
        //pthread_mutex_unlock(&tmp);
        callCommands(customer);
    }


}
void *banker_thread_run();
void *wake_neighbor(banker_customer *customer) {
    banker_customer *suivant = customer->next;

    pthread_mutex_lock(&register_mutex);
    while(suivant!= NULL && suivant->etat != WAITING) suivant = suivant->next;
    if(suivant!= NULL){
        suivant->etat = READY;
        //pthread_mutex_t tmp = current->head->mutex;
        //pthread_mutex_unlock(&tmp);
        pthread_mutex_unlock(&register_mutex);
        banker_thread_run();
        return NULL;
    }
    banker_customer *precedent = customer->prev;
    while(precedent!= NULL && precedent->etat != WAITING) precedent = precedent->prev;
    if(precedent!= NULL){
        precedent->etat = READY;
        //pthread_mutex_t tmp = current->head->mutex;
        //pthread_mutex_unlock(&tmp);
        pthread_mutex_unlock(&register_mutex);
        banker_thread_run();
        return NULL;
    }
    customer->etat = READY;
    pthread_mutex_unlock(&register_mutex);
    return NULL;
}
/**
* Parcours la liste de clients en boucle. Lorsqu'on en trouve un ayant fait une requête, on l'évalue et recommence
* l'exécution du client, au besoin
*
* @return
*/
void *banker_thread_run() {
    //printf("banker_thread_run\n");
    while(1){
        if(first == NULL){
            break;
        }
        banker_customer *current = first;
        pthread_mutex_lock(&register_mutex);
        while(current!= NULL && current->etat != READY) current = current->next;
        if(current!= NULL){
            current->etat = BANKER;
            pthread_mutex_unlock(&register_mutex);
            call_bankers(current);
            continue;
        }
        pthread_mutex_unlock(&register_mutex);
//        if((int)pthread_self() == thread_principal){
//            break;
//        }
    }
    return NULL;
}

// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------------------------------

/**
 * Cette fonction effectue une requête des ressources au banquier. Utilisez les mutex de la bonne façon, pour ne pas
 * avoir de busy waiting...
 *
 * @param customer le ticket client
 * @param cmd_depth la profondeur de la commande a exécuter
 * @return un code d'erreur
 */
error_code request_resource(banker_customer *customer, int cmd_depth) {
    //printf("RquestRessource\n");
    if(cmd_depth + 1 > customer->head->max_command){
        unregister_command(customer);
        return ERROR;
    }
    //pthread_mutex_t tmp = customer->head->mutex;
    //pthread_mutex_lock(&tmp);
    customer->depth = cmd_depth;
    customer->etat = READY;
    banker_thread_run();
    //request_resource(customer, cmd_depth++);

    return NO_ERROR;
}

/**
 * Utilisez cette fonction pour initialiser votre shell
 * Cette fonction est appelée uniquement au début de l'exécution
 * des tests (et de votre programme).
 */
error_code init_shell() {
    first = NULL;
    //printf("From the function, the thread id = %d\n", pthread_self());
    thread_principal = pthread_self();
    printf("Entrez la ligne de capacite commande:\n");
    char *line;
    if (HAS_ERROR(readLine(&line))) return ERROR;
    parse_first_line(line);

    nbRessourrce = conf->ressources_count;

    _available = malloc(nbRessourrce*sizeof(int));
    if(_available == NULL)
        return ERROR;
    for(int i = 0;i<nbRessourrce;i++){
        _available[i] = ressource_cap_max[i];
    }
    return NO_ERROR;
}

int nbThread;
pthread_t *arraythread;
int nbThreadInit;
/**
 * Utilisez cette fonction pour nettoyer les ressources de votre
 * shell. Cette fonction est appelée uniquement à la fin des tests
 * et de votre programme.
 */
void close_shell() {
    //printf("******close_shell****** \n",nbThreadInit );
    for(int i = 0;i<nbThreadInit;i++){
        pthread_cancel(arraythread[i]);
        free(arraythread[i]);
       // printf("pthread_cancel(&arrthread[%d]) :  Cancelled \n",i );
    }
    freeconfig();
    freeAndNextBanker();
    free(ressource_cap_max);
    free(_available);

    exit(0);
}

/**
 * Utilisez cette fonction pour y placer la boucle d'exécution (REPL)
 * de votre shell. Vous devez aussi y créer le thread banquier
 */
void *callThreadCommand(void* client){
    //printf("callThreadCommand\n");
    if(HAS_ERROR(request_resource(client,0))) ;
    //printf("Cest un thread\n");
    pthread_exit(pthread_self());

    //if(HAS_ERROR(unregister_command(client))) ;
}
void run_shell() {
    char *line;
    command_head *head;
    nbThreadInit = 0;
    nbThread = 10;
    arraythread = malloc(nbThread*sizeof(pthread_t*));
    if(arraythread == NULL){
        freeconfig();
        goto top;
    }
    while (1) {
        pthread_mutex_lock(&reading_mutex);
        if(nbThreadInit >= nbThread){
            printf("Reallocation\n");
            pthread_t *tmparrthread = realloc(arraythread,(nbThread+10)*sizeof(pthread_t*));
            nbThread += 10;
            arraythread = tmparrthread;

        }
        if (HAS_ERROR(readLine(&line))) goto top;
        if (strlen(line) == 0){
            pthread_mutex_unlock(&reading_mutex);
            continue;
        }
        if(!strcmp(line,"exit")){
            free(line);
            close_shell();
        }
        error_code code_error = create_command_chain(line, &head);
        if(code_error == -6){
            pthread_mutex_unlock(&reading_mutex);
            free(line);
            continue;
        }
        if (HAS_ERROR(code_error)) goto bot;
        free(line);
        banker_customer *client = register_command(head);
        if(client == NULL) goto bot;
        if(head->background){
            /** Ajouter une pthread */
            pthread_mutex_unlock(&reading_mutex);
            pthread_t t1;
            arraythread[nbThreadInit] = (pthread_t*) &t1;
            pthread_attr_t attr;
            pthread_attr_init(&attr);

            pthread_create(&t1,&attr,callThreadCommand,(void*) client);
            nbThreadInit++;
        }else{
            pthread_t t1;
            arraythread[nbThreadInit] = (pthread_t*) &t1;
            pthread_attr_t attr;
            pthread_attr_init(&attr);
            pthread_create(&t1,&attr,callThreadCommand,(void*) client);
            nbThreadInit++;

        }
    }


    bot:
    free(line);
    top:
    printf("An error has occured");
    exit(-1);

}

/**
 * Vous ne devez pas modifier le main!
 * Il contient la structure que vous devez utiliser. Lors des tests,
 * le main sera complètement enlevé!
 */
int main(void) {
    //printf("Tester\n");
    if (HAS_NO_ERROR(init_shell())) {

        run_shell();
        close_shell();
    } else {
        printf("Error while executing the shell.");
    }


}
