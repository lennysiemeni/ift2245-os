/**
Lenny SIEMENI 1055234
Mehran ASADI 1047847
 */
#include <stdint.h>
#include <stdio.h>

#include "tlb.h"

#include "conf.h"

struct tlb_entry
{
    unsigned int page_number;
    int frame_number;             /* Invalide si négatif.  */
    bool readonly : 1;
};

static FILE *tlb_log = NULL;
static struct tlb_entry tlb_entries[TLB_NUM_ENTRIES];

//https://stackoverflow.com/questions/23509348/how-to-set-all-elements-of-an-array-to-zero-or-any-same-value
//Sect. 9.4.4 "LRU page replacement" p.416-417 Operating System Concepts, Ninth Edition.
static unsigned int reference_string[TLB_NUM_ENTRIES] = {0};

static unsigned int tlb_hit_count = 0;
static unsigned int tlb_miss_count = 0;
static unsigned int tlb_mod_count = 0;


/* Initialise le TLB, et indique où envoyer le log des accès.  */
void tlb_init (FILE *log)
{
    for (int i = 0; i < TLB_NUM_ENTRIES; i++){
        tlb_entries[i].frame_number = -1;
    }
    tlb_log = log;
}

/******************** ¡ NE RIEN CHANGER CI-DESSUS !  ******************/

/* Recherche dans le TLB.
 * Renvoie le `frame_number`, si trouvé, ou un nombre négatif sinon.  */
static int tlb__lookup (unsigned int page_number, bool write)
{
    // TODO: COMPLÉTER CETTE FONCTION.
    int frame_number = -1;
    for (int i = 0; i< TLB_NUM_ENTRIES; i++){
        if (tlb_entries[i].page_number == page_number
            && tlb_entries[i].frame_number != -1){
            frame_number = tlb_entries[i].frame_number;
            reference_string[i] = reference_string[i] >> 1;
        }else{
            reference_string[i] = reference_string[i] >> 1;
        }
    }
    return frame_number;
}

/* Ajoute dans le TLB une entrée qui associe `frame_number` à
 * `page_number`.  */
static void tlb__add_entry (unsigned int page_number,
                            unsigned int frame_number, bool readonly)
{// TODO: COMPLÉTER CETTE FONCTION.

    int victim_page = 0;
    int i = 0;
    for ( i = 0; i < TLB_NUM_ENTRIES; i++){

        //cherche dans le TLB la page victime
        if (reference_string[i] < reference_string[victim_page]){
            victim_page = i ;
        }
    }

    tlb_entries[victim_page].page_number = page_number;
    tlb_entries[victim_page].frame_number = frame_number;
    tlb_entries[victim_page].readonly = readonly;

}

// Decalage du compteur sur les autres frames apres un sur le reference string:
// http://www.cburch.com/csbsju/cs/350/notes/21/
// (Sect. 9.4.4 "LRU page replacement" p.416-417 Operating System Concepts, Ninth Edition.)
void shift_other_frames(unsigned int page_number){
    for (int i = 0; i< TLB_NUM_ENTRIES; i++){
        if (tlb_entries[i].page_number != page_number ){
            reference_string[i] = reference_string[i] >> 1;
        }
    }
}

/******************** ¡ NE RIEN CHANGER CI-DESSOUS !  ******************/

void tlb_add_entry (unsigned int page_number,
                    unsigned int frame_number, bool readonly)
{
    tlb_mod_count++;
    tlb__add_entry (page_number, frame_number, readonly);
}

int tlb_lookup (unsigned int page_number, bool write)
{
    int fn = tlb__lookup (page_number, write);
    (*(fn < 0 ? &tlb_miss_count : &tlb_hit_count))++;
    return fn;
}

/* Imprime un sommaires des accès.  */
void tlb_clean (void)
{
    fprintf (stdout, "TLB misses   : %3u\n", tlb_miss_count);
    fprintf (stdout, "TLB hits     : %3u\n", tlb_hit_count);
    fprintf (stdout, "TLB changes  : %3u\n", tlb_mod_count);
    fprintf (stdout, "TLB miss rate: %.1f%%\n",
             100 * tlb_miss_count
             /* Ajoute 0.01 pour éviter la division par 0.  */
             / (0.01 + tlb_hit_count + tlb_miss_count));
}
