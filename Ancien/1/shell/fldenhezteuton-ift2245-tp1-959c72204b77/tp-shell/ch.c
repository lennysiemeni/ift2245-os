/* ch.c.
auteur:
date:
problèmes connus:

  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>

#define MIN_BUFFER_SIZE 256
#define BUFFERSIZE 200

enum operator{DO_OP, COND_OP, IF_OP, OR_OP, AND_OP, REDIRECT_OP, SUSPEND_OP, DONE_OP};

char * error="";
void removeSpaces();
int tolower(int c);

bool should_run_background(char *args);


bool should_run_background(char *args)
{
  bool background = false;
  int leng = strlen(args);
  if(strcmp(args[leng-1], "&") == 0)
  {
    args[leng-1] = NULL;
    background = true;
  }

  return background;
}



typedef struct command_node
{
  enum operator op;
  char * command;
  struct command_node* previous;
  struct command_node* next;
  int response;
} command_node;

enum token_type{COMMAND, IF, DO, DONE, OR, AND, REDIRECT, SUSPEND, END};

typedef struct
{
  enum token_type tok;
  char * text;
} s_token;

/*Forward declarations*/

s_token* tokenize(char *line);
command_node* parse(s_token tokens[]);
void interpret(command_node *ast);
int executeCommande(char *cmd);

s_token s(unsigned int*, s_token[]);
bool p(unsigned int*, s_token[]);
bool c(unsigned int*, s_token[]);
void consume(unsigned int*);

command_node* build_command_tree(s_token[]);

/*Function definitions*/

command_node* parse(s_token tokens[]){
  //printf("Parsing...\n");
  unsigned int iterator=0;
  error="";
  s_token result=s(&iterator, tokens);
  if((result.tok==END||result.tok==SUSPEND) && strlen(error)==0)
  {
    return build_command_tree(tokens);
  }
  else
  {
    if(strlen(error)==0)
    {
      char* error_buffer=malloc((strlen("Symbole inattendu: \0")+strlen(result.text)+1)*sizeof(char));
      strcat(error_buffer,"Symbole inattendu: ");
      error=strcat(error_buffer,result.text);
    }
    printf("Échec du parsing. Erreur: %s\n", error);
    if(error[0]=='S'||error[0]=='A'){
      free(error);
    }
    free(tokens);
    return NULL;
  }

}

command_node* new_node(s_token token, s_token next)
{
  command_node* present = NULL;
  switch(token.tok)
  {
    case COMMAND: present=malloc(sizeof(command_node));
                  present->command=token.text;
                  present->op=DO_OP;break;
    case IF: present=malloc(sizeof(command_node));
              present->command=next.text;
              present->op=COND_OP;break;
    case DO: present=malloc(sizeof(command_node));
                  present->op=IF_OP;
                  present->command=next.text;break;
    case DONE: present=malloc(sizeof(command_node));
                present->op=DONE_OP;break;
    case AND:present=malloc(sizeof(command_node));
                  present->op=AND_OP;
                  present->command=next.text;break;
    case OR:present=malloc(sizeof(command_node));
                  present->op=OR_OP;
                  present->command=next.text;break;
    case SUSPEND:present=malloc(sizeof(command_node));
                  present->op=SUSPEND_OP;
                  break;
    case REDIRECT:present=malloc(sizeof(command_node));
                  present->command=token.text;
                  present->op=REDIRECT_OP;break;
  }
  return present;
}

command_node* build_command_tree(s_token tokens[])
{
//  printf("Construction de la structure de commandes...\n");
  command_node* first=NULL;
  command_node* iterator=first;
  command_node* last=first;
  for(int i=0;tokens[i].tok!=END;++i)
  {
      if(iterator==first)
      {
        first=new_node(tokens[i],tokens[i+1]);
        first->previous=NULL;
        last=first;
        iterator=first->next;
      }
      else{
        last->next=new_node(tokens[i],tokens[i+1]); 
        iterator=last->next;
        (iterator)->previous=last;
        last=iterator; 
      }
      
      if(last->op==IF_OP||last->op==OR_OP||last->op==AND_OP || last->op==COND_OP)
      {
        ++i;
      }
  }
  free(tokens);
  return first;
}
/*
void free_command_tree(command_node* ast)
{
  if(ast==NULL)
    return;
  while(ast!=NULL && ast->next!=NULL){
  command_node* temp=ast->next;
  free(ast);
  ast=temp;
  }
  free(ast);
}
*/
/*
S -> P (B P)* A
P -> "IF" C (B C)* "DO" S ";DONE;" || C
C -> COMMAND || COMMAND ">" FILE
A -> "&"
B -> "||" || "&&"
*/

s_token s(unsigned int *iterator, s_token token_list[] )
{
  bool should_continue=p(iterator, token_list);
  while(should_continue && (token_list[*iterator].tok==AND || token_list[*iterator].tok==OR))
  {
    consume(iterator);
    should_continue=p(iterator, token_list);
  }
  return token_list[*iterator];

}

bool p(unsigned int *iterator, s_token token_list[]){
  if(token_list[*iterator].tok==IF)
  {
    consume(iterator);
    bool should_continue=c(iterator, token_list);
    while(should_continue && token_list[*iterator].tok==AND ||
     token_list[*iterator].tok==OR){
      consume(iterator);
      should_continue=c(iterator, token_list);
    }
    if(token_list[*iterator].tok==DO)
    {
      consume(iterator);
      s_token result=s(iterator,token_list);
      if(result.tok==DONE)
      {
        consume(iterator);
      }
      else {
        error="IF incomplet, manque le DONE";
        return false;
      }
    }
    else
    {
      error="IF incomplet, manque le DO";
      return false;
    }
  }
  else return c(iterator, token_list);
}

bool c(unsigned int* iterator, s_token token_list[])
{
  if(token_list[*iterator].tok==COMMAND){
    consume(iterator);
    if(token_list[*iterator].tok==REDIRECT){
      consume(iterator);
      consume(iterator);
    }
    return true;
  }
  else {
    char* error_buffer=malloc((40+strlen(token_list[*iterator].text)+1)*sizeof(char));
    strcat(error_buffer,"Attendait une commande, lu à la place ");
    error=strcat(error_buffer,(token_list[*iterator]).text);
    return false;
  }
}

void consume(unsigned int* iterator)
{
  (*iterator)++;
}



int executeCommande(char*cmd)
{
   int returnValue = 0;
    pid_t pid;
    pid = fork();

    if(pid < 0)
    {
      return pid;
    }
    if (pid == 0)
    {
    char** args=malloc(strlen(cmd)*sizeof(char*));
        args[0]=strtok(cmd," ");
        for(int i=1;i<strlen(cmd);++i)
        {
          args[i]=strtok(NULL," ");
        // printf("%s\n",args[i]);
          if(args[i]==NULL)
           break;
        }

        printf("Command %s : Arguments ",args[0]);
        for(int i=1;args[i]!=NULL;++i)
        {
          printf("%i %s",i,args[i]);

        }
        returnValue=execvp(args[0], args);
        return returnValue;
    }
    else {
        wait(&returnValue);
    }
    return returnValue;
}

int executeBackground(char*cmd)
{
  int returnValue = 0;
  pid_t pid;
  pid = fork();

  if(pid < 0)
  {
    return pid;
  }
  if (pid == 0)
  {
       char** args=malloc(strlen(cmd)*sizeof(char*));
        const char* command=strtok(cmd," ");
        for(int i=0;i<strlen(cmd);++i)
        {
          args[i]=strtok(NULL," ");
        // printf("%s\n",args[i]);
          if(args[i]==NULL)
           break;
        }
        printf("Command %s : Arguments ",command);
        for(int i=0;args[i]!=NULL;++i)
        {
          printf("%i %s",i,args[i]);

        }
      return execvp(command,args);
      
  }

  return 0;
}

void interpret(command_node *ast)
{
  if(ast==NULL)
  {
    return;
  }
  if(ast->op==DO_OP || ast->op==COND_OP)
  {
    ast->response = executeCommande(ast->command);
  }
  if(ast->op==IF_OP)
  {
    if(ast->previous->response ==0) 
    {
        ast->response=executeCommande(ast->command);
    }
  }
  if(ast->op==OR_OP)
  {
    if(ast->previous->response<0)
    {
      ast->response = executeCommande(ast->command);
    }
    //What's the difference between this and DO
  }
  if(ast->op==AND_OP)
  {
    if(ast->previous->response>-1) //Ca a bien rouler
    {
      ast->response = executeCommande(ast->command);
    }
  }
  if(ast->op==REDIRECT_OP)
  {
    close(1); // Release fd no - 1
    open(ast->command, "w"); // Open a file with fd no = 1
    // Child process
    if (fork() == 0)
    {
        executeCommande(ast->previous->command); // By default, the program writes to stdout (fd no - 1). ie, in this case, the file
    }
  }
  if(ast->op==SUSPEND_OP) //&: When you put everything in a background process
  {
    //sigprocmask()
    executeBackground(ast->command);
  }
  if(ast->next == NULL)
  {
    return;
  }

  interpret(ast->next);
}


// enum operator{DO_OP, IF_OP, OR_OP, AND_OP, REDIRECT_OP, SUSPEND_OP};
//
// typedef struct
// {
//   enum operator op;
//   char * command;
//   struct command_node* previous;
//   struct command_node* next;
//   int response;
// } command_node;




void removeSpaces(char* source)
{
  char* i = source;
  char* j = source;
  while(*j != 0)
  {
    *i = *j++;
    if(*i != ' ')
      i++;
  }
  *i = 0;
}

//strtok peut que separer sur un seul character.
//Pour contourner cela, cette fonction pourra chercher ou il ya des 'if' et les remplacer par un seul
//character special '@' et on pourrait tokenize sur ce character
//Tous notre logique est base sur le strtok donc on a besoin de une strategie qui fonctionne avec cela
void removeSubstrDelimitIf (char *string)
{
    size_t len = strlen(string);
    for(int i=0;i<len;i++)
    {
        int plusOne = 0;
        if(string[i] == 'i')
        {
            plusOne = i; plusOne+=1;
            if(string[plusOne] == 'f')
            {
                string[i] = ' ';
                string[plusOne] = '@';
                
            }
        }
    }
}

//Meme concept que removeSubstrDelimitIf mais pour Do
void removeSubstrDelimitDoDone (char *string)
{
    size_t len = strlen(string);
    int holder1;
    int holder2;
    for(int i=0;i<len;i++)
    {
        int plusOne = 0;
        if(string[i] == 'd')
        {
            plusOne = i; plusOne+=1;
            if(string[plusOne] == 'o')
            {
                holder1 = plusOne+1;
                holder2 = plusOne+2;
                if((string[holder1] == 'n') && (string[holder2] == 'e'))
                {
                    string[i] = ' ';
                    string[plusOne] = ' ';
                    string[holder1] = ' ';
                    string[holder2] = '~';
                  
                }
                else
                {
                    string[i] = ' ';
                    string[plusOne] = '^';
                   
                }
            }
        }
    }
}

int main (void)
{
	int bufsize = BUFFERSIZE;
  bool continuing = true;

  fprintf (stdout, "%% ");

  while(1)
  {
    printf("notre shell>");
    //char input_commands[bufsize];
	//shell operands: if, do, done, ;, &, &&, ||, >
    //Replace le scanf pas getline car ca pourrait allouer la memoire dynamiquement
    //scanf("%s",input_commands);


    char *line = NULL;
    ssize_t bufsize = 0;
    getline(&line, &bufsize, stdin);
    size_t len = strlen(line);

    //Lowercase le input pour ne pas devoir verifier pour IF/if/If/iF
    for(int i = 0; line[i]; i++)
    {
      line[i] = tolower(line[i]);
    }
  /*  printf( "Le input: %s\n", line);*/


	//Replace 'if' by '@'
    removeSubstrDelimitIf(line);
    //Replace 'do' by '^' et 'done' by '~'
    removeSubstrDelimitDoDone(line);

   //  printf( "Le input NEW: %s\n", line);

 /*   if((ifCount != doCount) || (doCount != doneCount))
    {
        printf( "Input non valid :(\n");
        continue;
    }*/

     char *token;
     char buff1[80];
     strcpy (buff1, "&&");
     char *cmdwda[len];
     int ctrwda = 0;
     int ctr=0;
     char *deliOne = "&&";


    //Tokenize les &&
	token = strtok(line, deliOne);
	while( token != NULL )
    {
        //removeSpaces(token);
        cmdwda[ctrwda] = token;
		token = strtok(NULL, deliOne);
        if(token != NULL)
        {
          ctr += 2;
          ctrwda += 1;
          cmdwda[ctrwda] = buff1;
          ctrwda += 1;
        }
	}


     //Tokenize les ||
     char *cmdwdo[len];
     int ctrwdo = 0;
     char *deliTwo = "||";
     int ctrTwo = 0;
     char buff2[80];
     strcpy (buff2, "||");

     //On doit louper sur un array qui contient des strings
     for(int i = 0; i<=ctr; i++)
     {
        token = strtok(cmdwda[i], deliTwo);
        while(token != NULL)
        {
          //removeSpaces(token);
          cmdwdo[ctrwdo] = token;
          token = strtok(NULL, deliTwo);
          if(token != NULL)
          {
            ctrTwo += 2;
            ctrwdo += 1;
            cmdwdo[ctrwdo] = buff2;
            ctrwdo += 1;
          }
          else
          {
            ctrTwo += 1;
            ctrwdo += 1;
          }
        }

      }

      //On a normalement un count de plus
      ctrTwo -= 1;


     char *cmdwif[len];
     int ctrwdif = 0;
     char *deliThree = "@";
     int ctrThree = 0;
     char buff3[80];
     strcpy (buff3, "if");



    //Bug si le input commence avec un if, cela se occupe de ce cas
     if(cmdwdo[0][0] == '@')
    {
        cmdwif[ctrwdif] = buff3;
        ctrwdif += 1;
        ctrThree += 1;
    }
     for(int i = 0; i<=ctrTwo; i++)
     {
        token = strtok(cmdwdo[i], deliThree);
        while(token != NULL)
        {
          //removeSpaces(token);
          cmdwif[ctrwdif] = token;
          token = strtok(NULL, deliThree);
          if(token != NULL)
          {
            ctrThree += 2;
            ctrwdif += 1;
            cmdwif[ctrwdif] = buff3;
            ctrwdif += 1;
          }
          else
          {
            ctrThree += 1;
            ctrwdif += 1;
          }
        }
      }
      ctrThree -= 1;



     char *cmdwdoer[len];
     int ctrwddoer = 0;
     char *deliFour = "^";
     int ctrFour = 0;
     char buff4[80];
     strcpy (buff4, "do");

     for(int i = 0; i<=ctrThree; i++)
     {
        token = strtok(cmdwif[i], deliFour);
        while(token != NULL)
        {
          //removeSpaces(token);
          cmdwdoer[ctrwddoer] = token;
          token = strtok(NULL, deliFour);
          if(token != NULL)
          {
            ctrFour += 2;
            ctrwddoer += 1;
            cmdwdoer[ctrwddoer] = buff4;
            ctrwddoer += 1;
          }
          else
          {
            ctrFour += 1;
            ctrwddoer += 1;
          }
        }
      }
      ctrFour -= 1;




     char *tokens[len];
     int ctrtokens = 0;
     char *deliFive = "~";
     int ctrFive = 0;
     char buff5[80];
     strcpy (buff5, "done");

     for(int i = 0; i<=ctrFour; i++)
     {
        token = strtok(cmdwdoer[i], deliFive);
        while(token != NULL)
        {
          //removeSpaces(token);
          tokens[ctrtokens] = token;
          token = strtok(NULL, deliFive);
          if(token != NULL)
          {
            ctrFive += 2;
            ctrtokens += 1;
            tokens[ctrtokens] = buff5;
            ctrtokens += 1;
          }
          else
          {
            ctrFive += 1;
            ctrtokens += 1;
          }
        }
      }

      ctrFive -= 1; //TODO: Change this and other -1's to free memory

      s_token* tokens_array =(s_token*)malloc((ctrFive+1)*sizeof(s_token));
      unsigned int iter=0;
      for(int j = 0; j<=ctrFive; j++)
      {
         if(strcmp(tokens[j],"if")==0)
                   {
            tokens_array[iter].tok=IF;
            tokens_array[iter].text=tokens[j];
            ++iter;
          }
        else if(strcmp(tokens[j],"do")==0)
                    {
            tokens_array[iter].tok=DO;
            tokens_array[iter].text=tokens[j];
            ++iter;
          }
        else if(strcmp(tokens[j],"done")==0)
                  {
            tokens_array[iter].tok=DONE;
            tokens_array[iter].text=tokens[j];
            ++iter;
          }
        else if(strcmp(tokens[j],"||")==0)
                    {
            tokens_array[iter].tok=OR;
            tokens_array[iter].text=tokens[j];
            ++iter;
          }
        else if(strcmp(tokens[j],"&&")==0)
                    {
            tokens_array[iter].tok=AND;
            tokens_array[iter].text=tokens[j];
            ++iter;
          }
        else if(strcmp(tokens[j],">")==0)
                    {
            tokens_array[iter].tok=REDIRECT;
            tokens_array[iter].text=tokens[j];
            ++iter;
          }
        else if(strcmp(tokens[j],"&")==0)
                    {
            tokens_array[iter].tok=SUSPEND;
            tokens_array[iter].text=tokens[j];
            ++iter;
          }
          else if(strcmp(tokens[j],"\0")==0||strcmp(tokens[j]," ")==0 ||
                  strcmp(tokens[j],"\n")==0)
          {}
          else
          {
            tokens_array[iter].tok=COMMAND;
            if(tokens[j][strlen(tokens[j])-1]=='\n')
              tokens[j][strlen(tokens[j])-1]='\0';
            tokens_array[iter].text=tokens[j];
            ++iter;
          }
      }

    tokens_array[iter].tok=END;
    tokens_array[iter].text="Fin de Ligne";
    iter++;

 /*   for(int i=0;i<iter;++i)
    {
      printf("%i token is type %i",i,tokens_array[i].tok);
      if(tokens_array[i].tok==COMMAND)
      {
        printf(" : %s",tokens_array[i].text);
      }
      printf("\n");
    }*/
    command_node* test=parse(tokens_array);
 /*   int node_counter=0;
    for(command_node* iterator=test;iterator!=NULL;++node_counter)
    {
      printf("Command node number %i :type %i command %s\n",node_counter,(iterator)->op,(iterator)->command);
      iterator=(iterator)->next;
    }
*/
    interpret(test);
    
  //  free_command_tree(test);
   /* s_token test_command_token, end_token;

    test_command_token.tok=COMMAND;
    test_command_token.text="qui est aux cieux";

    end_token.tok=END;
    end_token.text="Rien de rien";


    s_token very_special_tokens[3];

    very_special_tokens[0]=test_command_token;
    very_special_tokens[1]=end_token;
    very_special_tokens[2]=end_token;


    printf("tokenizer done \n");

    parse(very_special_tokens);

    very_special_tokens[1]=test_command_token;

    parse(very_special_tokens);*/
    //TODO: Bug!: When there is a done at the end it has one extra element in tokens array - a blank value
    //TODO: Bug!: When command starts with if, it does not recognize the first if
    //TODO: Bug!: Nothing to recognize and detect the & at the end of the command by itself - maybe look at tokenizeing but from the end of the string
	//TODO: return an array called tokens
	//first element of the array has to be a commande, second one a symbol, etc en alternance
	////////////////////////////////////////
    // shell operands: if, do, done, ;, &, &&, ||, >
    //getline(&input_commands,&length_commands,stdin);
    //char *tokens[] = tokenize(input_commands);
    //command_node *ast = parse(tokens);
    //interpret(ast);

  }

  fprintf (stdout, "Bye!\n");
  exit (0);
}
