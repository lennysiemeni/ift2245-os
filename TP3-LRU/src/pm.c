/**
Lenny SIEMENI 1055234
Mehran ASADI 1047847
 */
#include <stdio.h>
#include <string.h>
#include "conf.h"
#include "pm.h"
#include "pt.h"

static char *error_negative_address = "L'adresse obtenue est negative";
static char *error_address_outbounds = "L'adresse obtenue est hors limites";
static char *error_buffer = "Erreur lecture buffer";
static char *error_seek_set = "Erreur fseek";
static FILE *pm_backing_store;
static FILE *pm_log;
static char pm_memory[PHYSICAL_MEMORY_SIZE];
static unsigned int download_count = 0;
static unsigned int backup_count = 0;
static unsigned int read_count = 0;
static unsigned int write_count = 0;

// Initialise la mémoire physique
void pm_init (FILE *backing_store, FILE *log)
{
    pm_backing_store = backing_store;
    pm_log = log;
    memset (pm_memory, '\0', sizeof (pm_memory));
}

// Charge la page demandée du backing store
void pm_download_page (unsigned int page_number, unsigned int frame_number)
{
    download_count++;
    /* ¡ TODO: COMPLÉTER ! */
    //Met le pointeur au début de la page
    if (fseek(pm_backing_store, page_number * PAGE_FRAME_SIZE, SEEK_SET)){
        printf("%s",error_seek_set);
        return;
    }

    char buffer[PAGE_FRAME_SIZE + 1];  //null character
    memset(buffer, '0', PAGE_FRAME_SIZE + 1);

    //Lit les donnees et les met dans buffer
    if (fread(buffer, PAGE_FRAME_SIZE, 1, pm_backing_store) < 1){
        printf("%s",error_buffer);
        return;
    }

    unsigned int physical_memory_position = frame_number * PAGE_FRAME_SIZE;
    strncpy(&pm_memory[physical_memory_position],buffer,PAGE_FRAME_SIZE);
}

// Sauvegarde la frame spécifiée dans la page du backing store
void pm_backup_page (unsigned int frame_number, unsigned int page_number)
{
    //if page is dirty, backup the page
    if (pt_readonly_p(page_number)){
        backup_count++;
        /* ¡ TODO: COMPLÉTER ! */
        //Met le pointeur au début de la page
        if (fseek(pm_backing_store, page_number * PAGE_FRAME_SIZE, SEEK_SET)){
            printf("%s",error_seek_set);
            return;
        }

        char buffer[PAGE_FRAME_SIZE + 1];
        memset(buffer, '0', PAGE_FRAME_SIZE + 1);

        // Charge le contenu de la memoire physique pour le mettre dans le buffer
        unsigned int physical_memory_position = frame_number * PAGE_FRAME_SIZE;
        strncpy(buffer,&pm_memory[physical_memory_position],PAGE_FRAME_SIZE);

        //Ecrit les donnees du buffer dans le backing store
        if (fwrite(buffer, PAGE_FRAME_SIZE, 1, pm_backing_store) < 1){
            printf("%s",error_buffer);
            return;
        }
        pt_set_dirty(page_number,false);
    }
}

char pm_read (unsigned int physical_address)
{
    /* ¡ TODO: COMPLÉTER ! */
    //Check adresse negative
    if (physical_address < 0){
        printf("%s",error_negative_address);
    }
    //Check si l'adresse est pas en dehors du offset
    else if ((int)physical_address >= PHYSICAL_MEMORY_SIZE){
        printf("%s",error_address_outbounds);
    }else{
        read_count++;
        return pm_memory[physical_address];
    }
    return '!';
}

void pm_write (unsigned int physical_address, char c)
{
    /* ¡ TODO: COMPLÉTER ! */
    //Check adresse negative
    if (physical_address < 0){
        printf("%s",error_negative_address);
        return;
    }
        //Check si l'adresse est pas en dehors du offset
    else if ((int)physical_address >= PHYSICAL_MEMORY_SIZE){
        printf("%s",error_address_outbounds);
        return;
    }else{
        write_count++;
        pm_memory[physical_address] = c;
    }
}

void pm_clean (void)
{
    // Enregistre l'état de la mémoire physique.
    if (pm_log)
    {
        for (unsigned int i = 0; i < PHYSICAL_MEMORY_SIZE; i++)
        {
            if (i % 80 == 0)
                fprintf (pm_log, "%c\n", pm_memory[i]);
            else
                fprintf (pm_log, "%c", pm_memory[i]);
        }
    }
    fprintf (stdout, "Page downloads: %2u\n", download_count);
    fprintf (stdout, "Page backups  : %2u\n", backup_count);
    fprintf (stdout, "PM reads : %4u\n", read_count);
    fprintf (stdout, "PM writes: %4u\n", write_count);
}
