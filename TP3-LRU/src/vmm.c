/**
Lenny SIEMENI 1055234
Mehran ASADI 1047847
 */
#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#include "conf.h"
#include "common.h"
#include "vmm.h"
#include "tlb.h"
#include "pt.h"
#include "pm.h"

static unsigned int read_count = 0;
static unsigned int write_count = 0;
static FILE* vmm_log;

struct page_replacement {
    unsigned int page_number;
    int frame_number;
    bool valid_invalid_bit;
    unsigned int counter;

};

static struct page_replacement reference_string[NUM_FRAMES] = {{.counter=0}, {.valid_invalid_bit = true},{.page_number=0},{.frame_number=0}};
static bool is_initialized = false;

void vmm_init (FILE *log)
{
    // Initialise le fichier de journal.
    vmm_log = log;
}


// NE PAS MODIFIER CETTE FONCTION
static void vmm_log_command (FILE *out, const char *command,
                             unsigned int laddress, /* Logical address. */
                             unsigned int page,
                             unsigned int frame,
                             unsigned int offset,
                             unsigned int paddress, /* Physical address.  */
                             char c) /* Caractère lu ou écrit.  */
{
    if (out)
        fprintf (out, "%s[%c]@%05d: p=%d, o=%d, f=%d pa=%d\n", command, c, laddress,
                 page, offset, frame, paddress);
}

/* Loop, shift et initialise les bits*/
void applyLRU(int frameNumber){
    if (!is_initialized){
        for (int i = 0; i < NUM_FRAMES; ++i)
        {
            reference_string[i].frame_number  = i;
        }
        is_initialized = true;
    }
    for (int i = 0; i< NUM_FRAMES; i++){
        if (i == frameNumber){
            reference_string[i].counter =  reference_string[i].counter >> 1;
        }else{
            reference_string[i].counter =  reference_string[i].counter >> 1;
        }
    }
}
/* Effectue une lecture à l'adresse logique `laddress`.  */
char vmm_read (unsigned int laddress)
{
    char c = '!';
    read_count++;
    /* ¡ TODO: COMPLÉTER ! */

    // 256 bits / page --> 2^8 offset, 2^8 page number
    // Shift pour ne prendre que les bits de gauche (low-endian)
    // http://www.cburch.com/csbsju/cs/350/notes/21/ : Section "Replacement Algorithms"
    // LFU (Least frequently used)
    unsigned int pageNumber = laddress >> 8;

    int frameNumber = search_new_frame(pageNumber);
    applyLRU(frameNumber);
    //Concaténation à 0000000011111111
    int offset = laddress & 255;
    int addressPhysique = (frameNumber * PAGE_FRAME_SIZE) + offset;
    c = pm_read (addressPhysique);

    // TODO: Fournir les arguments manquants.
    vmm_log_command (stdout, "READING", laddress, pageNumber, frameNumber, offset, addressPhysique, c);
    return c;
}

/* Effectue une écriture à l'adresse logique `laddress`.  */
void vmm_write (unsigned int laddress, char c)
{
    write_count++;
    /* ¡ TODO: COMPLÉTER ! */

    unsigned int pageNumber = laddress >> 8;

    int frameNumber = search_new_frame_to_write(pageNumber);
    applyLRU(frameNumber);
    pt_set_dirty(pageNumber,true);
    int offset = laddress & 255;
    int addressPhysique = (frameNumber * PAGE_FRAME_SIZE) + offset;
    pm_write(addressPhysique, c);

    // TODO: Fournir les arguments manquants.
    vmm_log_command (stdout, "WRITING", laddress, pageNumber, frameNumber, offset, addressPhysique, c);
}

void update_ref_string(int frameNumber,unsigned int pageNumber,bool isEmpty){
    reference_string[frameNumber].frame_number = pageNumber;
    reference_string[frameNumber].valid_invalid_bit = isEmpty;
}

/* Cherche le compteur qui a la plus petite valeur parmis toutes les frames
 * et prends cette meme frame pour victime
 */
struct page_replacement getLRUFrame(){
    int victim_frame = 0;
    for (int i = 0; i< NUM_FRAMES; i++){
        if (reference_string[i].counter<reference_string[victim_frame].counter){
            victim_frame = i;
        }
    }
    return reference_string[victim_frame];
}

/* Get the next frame to read
Sauvegarde l'ancienne frame et charge la nouvelle frame*/
int getFrame(unsigned int pageNumber, bool readonly){
    struct page_replacement frame = getLRUFrame();
    int frameNumber = frame.frame_number;
    if (frame.valid_invalid_bit != true){
        pm_backup_page(frameNumber,frame.page_number);
        pt_unset_entry(frame.page_number);
    }
    pm_download_page(pageNumber,frameNumber);
    pt_set_entry(pageNumber,frameNumber);
    pt_set_readonly(pageNumber,readonly);
    update_ref_string(frameNumber,pageNumber,false);
    return frameNumber;
}

/* Cherche quelle frame victime a remplacer dans le TLB et la Page Table*/
int search_new_frame(unsigned int pageNumber){
    int frameNumber = tlb_lookup (pageNumber,false);
    if (frameNumber < 0){
        //TLB Miss
        frameNumber = pt_lookup(pageNumber);
        if (frameNumber < 0){
            //Page Fault
            frameNumber = getFrame(pageNumber,false);
        }
        tlb_add_entry(pageNumber,frameNumber,pt_readonly_p(pageNumber));
        shift_other_frames(pageNumber);
    }
    return frameNumber;
}

int search_new_frame_to_write(unsigned int pageNumber){
    int frameNumber;

    //Copy-on-write si on veut ecrire lors d'une lecture de frame (p.408 Operating System Concepts, Ninth Edition.)
    if (pt_readonly_p(pageNumber))
    {
        frameNumber = getFrame(pageNumber, true);
        tlb_add_entry(pageNumber,frameNumber,false);
        shift_other_frames(pageNumber);
    }

    else{
        frameNumber = search_new_frame(pageNumber);
    }

    return frameNumber;
}

// NE PAS MODIFIER CETTE FONCTION
void vmm_clean (void)
{
    fprintf (stdout, "VM reads : %4u\n", read_count);
    fprintf (stdout, "VM writes: %4u\n", write_count);
}