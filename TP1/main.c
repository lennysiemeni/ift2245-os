/*
auteurs : Mehran ASADI 1047847
          Lenny SIEMENI 1055234

 problemes rencontres :
 - atoi() cause des warning dans valgrind car en voie d'etre deprecie (conversion sans warning et clanky)
 - l'espace memoire alloue peut etre tres grand, il y a probablement meilleure solution, mais comme on est pas evalue sur l'optimisation mais
 plus sur les fuites de memoire, c'est donc notre approche (confirme avec Samuel)
  */
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>

// Forward declaration
int shell (void);
int lireSuiv(char** commande,int *size,int *indexMots,char *inputss);
int  parsern(char** commande,int *size, int *indexMots,char *inputs,int nbChar,int* finligne);
int parsefn(char *inputs,int nbChar,int* finligne);
int lireString(char **out,int* finligne);
int lireStringrn(char **out,int* finligne);
int parse_commande(char **args, int size, size_t *i, int isbg);

//r6(echo allo) && echo salut
int shell (void)
{
    int dim;
    int indexMots;
    int nbMots;
    int estBackg;
    char **commande;
    char *inputs = NULL;
    fprintf (stdout, "%% ");
    while(1){
        estBackg = 0;
        dim = 4096; //hard coded
        indexMots = 0;
        commande = malloc((dim) * sizeof(*commande));
        if (commande == NULL){
            printf("Malloc failed \n");
            return -1;
        }

        nbMots = lireSuiv(commande,&dim,&indexMots,inputs);
        if(nbMots == -1){
            break;
        }
        if(nbMots == 0){
            fprintf (stdout, "%% ");
            continue;
        }
        if (strcmp(*(commande + nbMots-1), "&") == 0){
            *(commande + nbMots-1) = NULL;
            estBackg = 1;
        }
        *(commande + nbMots) = NULL;
        size_t i = 0;
        nbMots++;
        parse_commande(commande, nbMots, &i, estBackg);
        fprintf (stdout, "%% ");
    }

    fprintf (stdout, "Bye!\n");
    exit (0);
}
/*
 * Parseur : Style du parseur et grammaire inspire du cours de compilation IFT3065
 * Parseur a descente recursive
 */
int lireStringrn(char **out,int* finligne){
    *finligne = 0;
    char c = getc(stdin);
    //printf("%c\n",c);

    while(c == ' '){
        c = getc(stdin);
    }
    if(c == '\n' || c == '\0'){
        *finligne = 1;
        return 0;
    }
    size_t nbChar = 5;
    int parouvrant = 0;
    int parfermant = 0;

    int indexChar = 0;
    *out = (char*)malloc(nbChar*sizeof(char)+1);
    if (*out == NULL){
        printf("Malloc failed \n");
        return -1;
    }
    while(1){
        if(c == '\n' || c == '\0'){
            *finligne = 1;
            break;
        }
        if(indexChar == nbChar){
            nbChar = nbChar *2;
            *out = realloc(*out,nbChar);
            if (*out == NULL){
                printf("Malloc failed \n");
                return -1;
            }
        }
        if(c == '(')
            parouvrant++;
        if(c == ')')
            parfermant++;
        if((parfermant - parouvrant)>0){
            break;
        }
        *(*out+indexChar) = c;
        indexChar++;
        c = getc(stdin);
    }
    *(*out+indexChar) = '\0';
    return indexChar;
}
/*
* Function: lireLigne
*/
int lireString(char **out,int* finligne){
    *finligne = 0;
    char c = getc(stdin);
    //printf("%c\n",c);

    while(c == ' '){
        //printf("boucle\n");
        c = getc(stdin);
    }
    if(c == '\n' || c == '\0'){
        *finligne = 1;
        return 0;
    }
    size_t nbChar = 5;
    int indexChar = 0;
    *out = (char*)malloc(nbChar*sizeof(char)+1);
    if (*out == NULL){
        printf("Malloc failed \n");
        return -1;
    }
    while(c  != ' '){
        if(c == '\n' || c == '\0'){
            *finligne = 1;
            break;
        }
        if(indexChar == nbChar){
            nbChar = nbChar *2;
            *out = realloc(*out,nbChar);
            if (*out == NULL){
                printf("Malloc failed \n");
                return -1;
            }
        }
        *(*out+indexChar) = c;
        indexChar++;
        c = getc(stdin);
        //printf("%c\n",c);
    }
    *(*out+indexChar) = '\0';
    return indexChar;
}

// Fonction permettant de parser la fonction fN
int parsefn(char *inputs,int nbChar,int* finligne){
    int index = 1;
    int nombreInt = 0;
    int nbString = 5;
    int indexString = 0;
    char *nombre = (char*)malloc(nbString * sizeof(char));
    while(isdigit(*(inputs+index))!=0 && index <nbChar){
        if(indexString == nbString){
            nbString = nbString * 2;
            nombre = (char *)realloc(nombre,nbString);
        }
        *(nombre+indexString) = *(inputs+index);
        indexString++;
        index++;

    }
    if(index <nbChar && *(inputs+index) == '('){
        //printf("parrenthese trouver\n");
        index++;
        int nbreastant = nbChar - index;
        if(nbreastant > 1){
            char *commandrn = (char*)malloc((nbreastant+1) * sizeof(char));
            int rnindex = 0;
            while(index <nbChar){
                *(commandrn+rnindex) = *(inputs+index);
                rnindex++;
                index++;
            }
            *(commandrn+nbreastant) = '\0';
            char* inputrn = NULL;
            int nbargrn = lireStringrn(&inputrn,finligne);
            if(nbargrn == -1){
                printf("errorMalloc");
                return -1;
            }
            if(nbargrn == 0){
                printf("errorCommand");
                return - 1;
            }

            nombreInt = atoi(nombre);

            free(inputrn);
            return nombreInt;
        }
    }
    return 0;
}

// Fonction permettant de parser la fonction rN
int parsern(char** commande,int *size, int *indexMots,char *inputs,int nbChar,int* finligne){
    int index = 1;
    int nombreInt = 0;
    int nbString = 5;
    int indexString = 0;
    char *nombre = (char*)malloc(nbString * sizeof(char));
    while(isdigit(*(inputs+index))!=0 && index <nbChar){
        if(indexString == nbString){
            nbString = nbString * 2;
            nombre = (char *)realloc(nombre,nbString);
        }
        *(nombre+indexString) = *(inputs+index);
        indexString++;
        index++;

    }
    if(index <nbChar && *(inputs+index) == '('){
        //printf("parrenthese trouver\n");
        index++;
        int nbreastant = nbChar - index;
        if(nbreastant > 1){
            char *commandrn = (char*)malloc((nbreastant+1) * sizeof(char));
            int rnindex = 0;
            while(index <nbChar){
                *(commandrn+rnindex) = *(inputs+index);
                rnindex++;
                index++;
            }
            *(commandrn+nbreastant) = '\0';
            char* inputrn = NULL;
            int nbargrn = lireStringrn(&inputrn,finligne);
            if(nbargrn == -1){
                printf("errorMalloc");
                free(nombre);
                return -1;
            }
            if(nbargrn == 0){
                printf("errorCommand");
                free(nombre);
                return - 1;
            }

            nombreInt = atoi(nombre);//source : atoi() -  https://www.manpagez.com/man/3/atoi/ - https://stackoverflow.com/questions/781668/char-to-int-conversion-in-c

            if(nombreInt >0){
                *(commande + *indexMots) = commandrn;
                *indexMots = *indexMots + 1;
                *(commande + *indexMots) = inputrn;
                if(nombreInt >1 ){
                    *indexMots = *indexMots + 1;
                    *(commande + *indexMots) = "&rN&\0";
                }
            }
            free(nombre);
            return nombreInt;
        }
    }
    return 0;
}

int lireSuiv(char** commande,int *size,int *indexMots,char *inputss) {
    int finligne = 0;
    int nbChar = 0;
    char *inputs;
    while(1) {
        if (*size == *indexMots) {
            *size = *size * 2;
            *commande = realloc(*commande, (*size+1) * sizeof(*commande));
        }
        nbChar = lireString(&inputs,&finligne);
        if (nbChar == -1) {
            return -1;
        }
        if (nbChar == 0) {
            return *indexMots;
        }
        if(*inputs == 'f'){
            int nbrn = parsefn(inputs,nbChar,&finligne);
            if (nbrn == -1)
                return -1;
            if (nbrn >0 ){
                *(commande + *indexMots) = "&fN&\0";
                *indexMots = *indexMots + 1;
                continue;
            }
        }
        if(*inputs == 'r'){
            if (*size <= *indexMots+3) {
                *size = *size * 2;
                *commande = realloc(*commande, (*size+1) * sizeof(*commande));
            }
            int nbrn = parsern(commande,size,indexMots,inputs,nbChar,&finligne);
            if (nbrn == -1)
                return -1;
            if(nbrn > 0){
                if(nbrn == 1){
                    *indexMots = *indexMots + 1;
                    continue;
                }
                //printf("mot afficher:%s\n", *(commande + *indexMots));
                char* tmparray[2];
                int  varind= *indexMots-1;
                int commind = *indexMots-2;
                tmparray[1] = *(commande + *indexMots);
                tmparray[0] = *(commande + *indexMots-1);
                //printf("command afficher:%s\n", tmparray[0]);
                //printf("dim afficher:%d\n", (int)strlen(*(commande + *indexMots)));
                int nbtmp = (nbrn * 3) - 1;
                int nbtmpdeux =  *size - *indexMots - nbtmp;
                if(nbtmpdeux < 2){
                    if(nbtmpdeux < 0)
                        nbtmpdeux*= -1;
                    //printf("nombre manquant: %d\n",nbtmpdeux);
                    int nbtmptrois = *size + nbtmpdeux + 1;
                    while(*size < nbtmptrois)
                        *size = *size * 2;
                    *commande = realloc(*commande, (*size+1) * sizeof(*commande));
                }
                for(int i = 0;i<nbrn-2;i++){
                    *(commande + *indexMots) = *(commande + commind);
                    *indexMots = *indexMots + 1;
                    *(commande + *indexMots) = *(commande + varind);
                    *indexMots = *indexMots + 1;
                    *(commande + *indexMots) = "&rN&\0";
                    *indexMots = *indexMots + 1;
                }
                *(commande + *indexMots) = *(commande + commind);
                *indexMots = *indexMots + 1;
                *(commande + *indexMots) = *(commande + varind);
                *indexMots = *indexMots + 1;
                if(finligne>0){
                    return *indexMots;
                }
                continue;
            }


        }

        *(commande + *indexMots) = inputs;
        *indexMots = *indexMots + 1;
        if(finligne>0){
            return *indexMots;
        }
    }
}

/*
* Fonction: execute
 * Execute une commande entree en passant les bons parametres a execvp()
* returns: status
 * // Squelette inspire des fichiers de la demo
*/
int execute(char *args[], int size, int isbg){
    pid_t pid;
    pid_t pid_return_val;
    int status;
    pid = fork();
    if(pid < 0){
        return 1;
    }
    else if(pid == 0) {
        /*child process*/
        if(isbg){
            setpgid(pid, 0);
            size_t ii = 0;

            exit(parse_commande(args, size, &ii, 0));
        }
        execvp(args[0], args);

        printf("%s: command not found\n", args[0]);
        exit(1);
    }
    else{
        // https://stackoverflow.com/questions/33508997/waitpid-wnohang-wuntraced-how-do-i-use-these/34845669
        /*parent process*/
        if(isbg){
            setpgid(pid, 0);
            pid_return_val = waitpid(0, &status, WNOHANG);
        } else{
            pid_return_val = waitpid(pid, &status, WUNTRACED);
        }
    }
    return status;
}
/*
* Function: parse_commande
* analyse les token (mots) qui ont ete parse et applique l'execution des parametres dans l'ordre
*/
int parse_commande(char **args, int size, size_t *i, int isbg){
    int status = 0;
    size_t j = 0;
    char *tempcommand[size];

    while (*i < size){
        if (args[*i] == NULL){
            // On arrive a la fin de la ligne de commande
            tempcommand[j] = args[*i];
            status = execute(tempcommand, size, isbg);
            return status;

        } else if (strcmp(args[*i], "&&" ) == 0 && !isbg){

            tempcommand[j] = NULL; // NULL pour execvp
            (*i)++;
            status = execute(tempcommand, size, isbg);
            if(status == 0){
                // Le premier argumetn du AND a reussit on analyse le second
                return parse_commande(args, size, i, isbg);
            }
        } else if (strcmp(args[*i], "||") == 0 && !isbg){
            tempcommand[j] = NULL; // NULL pour execvp
            (*i)++;
            status = execute(tempcommand, size, isbg);
            if(status != 0){
                // Si le premier argument (seulement si) du OR a echoue, on analyse le second
                return parse_commande(args, size, i, isbg);
            }
            break;
        } else if (strcmp(args[*i], "&rN&") == 0 && !isbg){
            tempcommand[j] = NULL; // NULL pour execvp
            (*i)++;
            status = execute(tempcommand, size, isbg);
            return parse_commande(args, size, i, isbg);

        }else if (strcmp(args[*i], "&fN&") == 0 && !isbg){
            tempcommand[j] = NULL; // NULL pour execvp
            (*i)++;
            return parse_commande(args, size, i, isbg);

        }else {
            tempcommand[j] = args[*i];
            (*i)++;
            j++;
        }
    }
    return status;
}
int main(void){
    return shell();
}