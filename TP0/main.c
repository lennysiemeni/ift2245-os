/**
 * Auteurs : Lenny SIEMENI 1055234
 *           Mehran ASADI  1047847
 *
 */

#include <stdlib.h>
#include <stdio.h>

typedef unsigned char byte;
typedef int error_code;

#define ERROR (-1)
#define HAS_ERROR(code) ((code) < 0)
#define HAS_NO_ERROR(code) ((code) >= 0)

/**
 * Cette fonction compare deux chaînes de caractères.       
 * @param p1 la première chaîne
 * @param p2 la deuxième chaîne
 * @return le résultat de la comparaison entre p1 et p2. Un nombre plus petit que
 * 0 dénote que la première chaîne est lexicographiquement inférieure à la deuxième.
 * Une valeur nulle indique que les deux chaînes sont égales tandis qu'une valeur positive
 * indique que la première chaîne est lexicographiquement supérieure à la deuxième.
 */
error_code strcmp(char *p1, char *p2) {
    char *s1 = (char *) p1;
    char *s2 = (char *) p2;
    char c1, c2;
    do {
        c1 = (char) *s1++;
        c2 = (char) *s2++;
        if (c1 == '\0')
            return c1 - c2;
    } while (c1 == c2);
    return c1 - c2;
}

/**
 * Ex. 1: Calcul la longueur de la chaîne passée en paramètre selon
 * la spécification de la fonction strlen standard
 * @param s un pointeur vers le premier caractère de la chaîne
 * @return le nombre de caractères dans le code d'erreur, ou une erreur
 * si l'entrée est incorrecte
 */
error_code strlen(char *s) {
    long int l = 0;

    if(*s == '\0'){ //si la chaine n'est pas vide
        return ERROR;
    }

    if (s != NULL){
        while(*s != '\0'){
            s++;
            l++;
        }
        return l;
    } else {
        return ERROR;
    }
}

/**
 * Ex.2 :Retourne le nombre de lignes d'un fichier sans changer la position
 * courante dans le fichier.
 * @param fp un pointeur vers le descripteur de fichier
 * @return le nombre de lignes, ou -1 si une erreur s'est produite
 */
error_code no_of_lines(FILE *fp) {
    int nbLignes = 0;
    long size = ftell(fp);
    if (fp == NULL) {
        printf("File not found \n");
        return ERROR; // File not found
    } else {
        int ch = getc(fp);
        while (ch != EOF) {
            if (ch == '\n') {
                nbLignes++;
            }
            ch = getc(fp);
        }
        fseek(fp, size, SEEK_SET);
        return nbLignes;
    }
}

/**
 * Ex.3: Lit une ligne au complet d'un fichier
 * @param fp le pointeur vers la ligne de fichier
 * @param out le pointeur vers la sortie
 * @param max_len la longueur maximale de la ligne à lire
 * @return le nombre de caractère ou ERROR si une erreur est survenue
 */
error_code readline(FILE *fp, char **out, size_t max_len) {

    // Est-ce que fp peut etre deplace ici ?-> On a suppose que oui)
    if (fp == NULL) {
        printf("File not found \n");
        return ERROR;
    }
    *out = (char*)malloc(max_len*sizeof(char)+1);
    if (*out == NULL){
        printf("Malloc failed \n");
        return ERROR;
    }
    int i = 0;
    *(*out+i) = getc(fp);
    while ( *(*out+i) != '\n' && *(*out+i) != '\0') {
        i++;
        if (i == max_len) {
            printf("Not enough memory \n");
            return -2;
        }
        *(*out+i) = getc(fp);
    }
    *(*out+i)='\0';

    return i;
}

/**
 * Ex.4: Structure qui dénote une transition de la machine de Turing
 */
typedef struct {
    char *current_state;
    char *next_state;
    int relative_movement;
    char symbol_to_read;
    char symbol_to_write;
} transition;

/**
 * Ex.5: Copie un bloc mémoire vers un autre
 * @param dest la destination de la copie
 * @param src  la source de la copie
 * @param len la longueur (en byte) de la source
 * @return nombre de bytes copiés ou une erreur s'il y a lieu
 */
void* memcpy(void *dest, void *src, size_t len) {
    char *d = dest;
    char *s = src;
    // Check si la valeur est -1
    int i = 0;
    while (i<=len){
        d[i] = s[i];
        i++;
    }
    return dest;
}

/**
 * Ex.6: Analyse une ligne de transition
 * @param line la ligne à lire
 * @param len la longueur de la ligne
 * @return la transition ou NULL en cas d'erreur
 */
transition *parse_line(char *line, size_t len) {
    //printf("ligne: %s\n",line);
    if(line[0]!='('){
        return NULL;
    }
    transition *transition_instruction = malloc(sizeof(transition));

    transition_instruction->current_state = (char*)malloc((5+1)*sizeof(char));
    transition_instruction->next_state = (char*)malloc((5+1)*sizeof(char));
    for (size_t i = 0; i < 6; ++i) {
        transition_instruction->current_state[i] = NULL;
        transition_instruction->next_state[i] = NULL;
    }
    int index = 0;
    index++; //passer le premier (
    int transindex = 0;
    while(line[index]!=','){
        *(transition_instruction->current_state+transindex) =  line[index];
        line++;
        transindex++;
    }
    transindex = 0;
    index++; //passer le premier ,
    transition_instruction->symbol_to_read = line[index];
    while(line[index]!='('){
        index++;
    }
    index++; //passer le premier (
    while(line[index]!=','){
        *(transition_instruction->next_state+transindex) = line[index];
        line++;
        transindex++;
    }
    transindex = 0;
    index++; //passer le premier ','
    transition_instruction->symbol_to_write = line[index];
    index += 2; //passer le deuxième ','
    switch(line[index])
    {
        case 'D':
            transition_instruction->relative_movement = 1;
            break;
        case 'G':
            transition_instruction->relative_movement = -1;
            break;
        case 'R':
            transition_instruction->relative_movement = 0;
            break;

            // movement doesn't match any case constant D, G, R
        default:
            printf("Error! operator is not correct");
    }
    return transition_instruction;
}

/**
 * Ex.7: Execute la machine de turing dont la description est fournie
 * @param machine_file le fichier de la description
 * @param input la chaîne d'entrée de la machine de turing
 * @return le code d'erreur
 */
error_code execute(char *machine_file, char *input) {
    int nbChar = strlen(input);
    printf("input : %s \n", input);
    printf("input : %d \n", nbChar);
    int maximum = nbChar;
    printf("maximum: %d\n",maximum);
    char *ruban = (char*)malloc((maximum+1)*sizeof(char));
    if (ruban == NULL){
        return ERROR;
    }
    for(int ii=0;ii<(maximum+1);++ii){
        ruban[ii] = NULL;
    }
    memcpy(ruban,input,nbChar);
    printf("Charactere: %c\n",ruban[nbChar]);
    for(int ii=nbChar;ii<maximum;++ii){
        ruban[ii] = ' ';
    }
    printf("CharactereApres: %c\n",ruban[nbChar]);
    printf("ruban: %s\n",ruban);
    FILE *fp;
    char *outputStream;
    fp = fopen(machine_file, "r");
    int nb_instructions = no_of_lines(fp);
    printf("Nombre de caracteres: %d \n", nb_instructions);
    transition *array_of_instructions[nb_instructions-3];
    //array_of_instructions = (transition*)malloc((nb_instructions-3)*sizeof(transition));
//    for (int i = 0; i < nb_instructions-3; i++) {
//        array_of_instructions[i] = (transition*)malloc(sizeof(transition));
//    }


    int nb_ch=0;
    int skip = 0;
    while(skip <= 2){
        nb_ch = readline(fp, &outputStream, 64);
        printf("nb_ch: %d \n",nb_ch);
        free(outputStream);
        skip++;
    }
    nb_instructions -= 3;
    for(int i=0;i<nb_instructions;++i){
        readline(fp, &outputStream, 64);

        array_of_instructions[i] = parse_line(outputStream, 64);
        free(outputStream);

    }

    int index = 0;
    int i = 0;
    char* etatActuel = "S";
    while(1){
        int nonTrouver = 1;
        while(i<nb_instructions){
            if(!strcmp(array_of_instructions[i]->current_state,etatActuel)){
                if(array_of_instructions[i]->symbol_to_read == ruban[index]){
                    ruban[index] = array_of_instructions[i]->symbol_to_write; ///Remplace valeur sur ruban
                    printf("(EtatInit: %s)   (EtatSuiv: %s)   (Direction: %d)\n",etatActuel,array_of_instructions[i]->next_state,array_of_instructions[i]->relative_movement);
                    printf("symbole: %c\n",ruban[index]);
                    etatActuel = array_of_instructions[i]->next_state; ///Changement etat
                    index += array_of_instructions[i]->relative_movement; ///Changer indexRuban
                    printf("Index: %d\n",index);
                    printf("Ruban: %s\n",ruban);
                    nonTrouver = 0;
                    break;
                }
            }
            i++;
        }
        i=0;
        if(!strcmp(etatActuel,"A")){
            printf("EtatAccepter\n");
            for (int j = 0; j < nb_instructions; ++j) {
                free(array_of_instructions[j]->next_state);
                free(array_of_instructions[j]->current_state);
                free(array_of_instructions[j]);

            }
            free(ruban);
            //free(outputStream);
            return 1;
        }
        if(!strcmp(etatActuel,"R")){
            printf("EtatRejeter\n");
            for (int j = 0; j < nb_instructions; ++j) {
                free(array_of_instructions[j]->next_state);
                free(array_of_instructions[j]->current_state);
                free(array_of_instructions[j]);
            }
            free(ruban);
            //free(outputStream);
            return 0;
        }
        if(index == maximum ){
            int newMax = maximum*2;

            ruban = realloc(ruban,newMax+1);
            if(ruban == NULL){
                break;
            }
            for(int ii=maximum;ii<newMax+1;++ii){
                ruban[ii] = NULL;
            }
            for(int ii=maximum;ii<newMax;++ii){
                ruban[ii] = ' ';
            }
            printf("------------------------------\n");
            printf("|          Realloc           |\n");
            printf("------------------------------\n");
            maximum = newMax;
        }
        if(index==-1 || nonTrouver){

            printf("error\n");
            break;
        }

    }

    for (int j = 0; j < nb_instructions; ++j) {
        free(array_of_instructions[j]->next_state);
        free(array_of_instructions[j]->current_state);
        free(array_of_instructions[j]);
    }
    free(ruban);
    free(outputStream);
    return ERROR;
}


int main() {
// Vous pouvez ajouter des tests pour les fonctions ici
//
//    printf("/** --------------- Test sur le nombre de caracteres dans une chaine de caractere --------------- **/\n");
//    printf("Nombre de caracteres dans la chaine 'test' : %d\n", strlen("test"));
//    printf("Nombre de caracteres dans la chaine '!@#&$*(^)&) : %d'\n", strlen("!@#&$*(^)&)"));
//    printf("Nombre de caracteres dans la chaine '784651328543158547849868' : %d\n", strlen("784651328543158547849868"));
//    printf("Nombre de caracteres dans la chaine '-1': %d\n", strlen("-1"));
//    printf("Nombre de caracteres dans la chaine (NULL_TERMINATOR): %d\n", strlen("\0")); // a fixer ?
//    printf("\n\n");
//
//    printf("/** --------------- Test sur le nombre de lignes contenues dans un fichier .txt passe en parametre --------------- **/\n");
    FILE *fp;
//    fp = fopen("../power_len.txt", "r");
//    printf("Nombre de lignes dans le fichier 'power_len.txt' : %d\n", no_of_lines(fp));
//    fp = fopen("../file.txt", "r");
//    printf("Nombre de lignes dans le fichier 'file.txt' : %d\n", no_of_lines(fp));
//    fp = fopen("../simple.txt", "r");
//    printf("Nombre de lignes dans le fichier 'simple.txt' : %d\n", no_of_lines(fp));
//    printf("\n\n");
//
//    printf("/** --------------- Test sur la lecture d'une ligne dans un fichier .txt passe en parametre --------------- **/\n");
//    char *test_outputStream;
//    test_outputStream = malloc(sizeof(long*));
//    fp = fopen("../file.txt", "r");
//    int dist = readline(fp, &test_outputStream, 128);
//    printf("Nombre de caracteres dans 'file.txt' : %d \n",dist );
//    printf("OutputStream : %s\n",test_outputStream);
//
//
//    fp = fopen("../power_len.txt", "r");
//    dist = readline(fp, test_outputStream, 128);
//    printf("Nombre de caracteres dans 'power_len.txt' : %d \n", dist);
//    printf("OutputStream : %s \n", test_outputStream[0]);
//
//    fp = fopen("../test_long_line.txt", "r");
//    dist = readline(fp, test_outputStream, 2048);
//    printf("Nombre de caracteres dans 'test_long_line.txt' : %d \n", dist);
//    printf("OutputStream : %s \n\n", test_outputStream[0]);
//
//
//    printf("/** --------------- Test sur la copie d'un bloc memoire vers un autre --------------- **/\n");
//    void* destination_test;
//    destination_test = malloc(sizeof(int)*48);
//    char text_test[48]= "Une chaine de caractere de la machine de turing";
//    printf("memcpy result: %s \n", memcpy(destination_test, text_test,48));
//
//    printf("/** --------------- Test de lecture d'une ligne de transition --------------- **/\n");
//    transition *test_t;
//    test_t = malloc(sizeof(transition));
//    fp = fopen("../test_transition_parsing.txt", "r");
//    dist = readline(fp, test_outputStream, 64);
//    test_t = parse_line(test_outputStream[0], dist);
//
//    printf("Etat : %s \n", test_t->current_state);
//    printf("Movement : %d \n", test_t->relative_movement);
//    printf("Symbol a lire : %c \n", test_t->symbol_to_read);
//    printf("Symbole a ecrire : %c \n", test_t->symbol_to_write);
//    free(test_outputStream);
//    free(destination_test);

//    printf("/** --------------- La machine de Turing --------------- **/\n");
    char *fichier = "../power_len.txt";
    char *entree = "01010101";
    execute(fichier,entree);

    return 0;
}