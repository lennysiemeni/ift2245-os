/* fichier: "petit-comp.c" */
/* Un petit compilateur et machine virtuelle pour un sous-ensemble de C.  */
/*Auteur : Lenny SIEMENI (1055234) */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>

/*---------------------------------------------------------------------------*/
jmp_buf ptctrl;
char *msg;
int l_ptctrl = 1;

void deleteTree(); //forward declaration
/* Analyseur lexical. */

enum { DO_SYM, ELSE_SYM, IF_SYM, WHILE_SYM, PRINT_SYM, BREAK_SYM, CONTINUE_SYM, GOTO_SYM, LBRA, RBRA, LPAR,
       RPAR,LSQBRA_SYM, RSQBRA_SYM, PLUS, MINUS, MULT_SYM, DIV_SYM, PERCENT_SYM, LESS_SYM, LESS_THAN_EQUAL_SYM,GREATER_SYM, GREATER_THAN_EQUAL_SYM, SEMI, NOT_EQUAL_SYM,EQUAL_SYM, EQUIVALENT_SYM, INT, ID, EOI, COLON_SYM }; //ajout de plusieurs symbols

char *words[] = { "do", "else", "if", "while", "print", "break", "continue", "goto", NULL }; //ajout des nouvelles etiquettes

int ch = ' ';
int sym;
int int_val;
int nb_blocs_alloues;
char id_name[100];


void syntax_error(int error_number) { 
  switch (error_number)
  {
  case 0: fprintf(stderr, "Erreur de syntaxe 0: symbole '=' manquant \n "); break;
  case 1: fprintf(stderr, "Erreur de syntaxe 1: symbole '(' manquant \n "); break;
  case 2: fprintf(stderr, "Erreur de syntaxe 2: symbole ')' manquant \n "); break;
  case 3: fprintf(stderr, "Erreur de syntaxe 3: symbole '[' manquant \n "); break;
  case 4: fprintf(stderr, "Erreur de syntaxe 4: symbole ']' manquant \n "); break;
  case 5: fprintf(stderr, "Erreur de syntaxe 5: symbole '{' manquant \n "); break;
  case 6: fprintf(stderr, "Erreur de syntaxe 6: symbole '}' manquant \n "); break;
  case 7: fprintf(stderr, "Erreur de syntaxe 7: symbole ';' manquant \n "); break;
  case 8: fprintf(stderr, "Erreur de syntaxe 8: symbole ':' manquant \n "); break;
  case 9: fprintf(stderr, "Erreur de syntaxe 9 : production ou argument manquant, cette phrase n'est pas conforme a la grammaire du langage \n "); break;
  case 10: fprintf(stderr, "Erreur de syntaxe 10: Argument illegal \n "); break;
  default: fprintf(stderr, "Erreur de syntaxe -1: Erreur inconnue\n "); break;
  }
  
  deleteTree();
  exit(1); 
}

void next_ch() { ch = getchar(); }

void next_sym()
{
  int count = 0;//Ajout d'un drapeau preventif pour grandes valueurs
  while (ch == ' ' || ch == '\n' || ch == '\t') next_ch(); l_ptctrl++;
  switch (ch)
    { case '{': sym = LBRA;  next_ch(); break;
      case '}': sym = RBRA;  next_ch(); break;
      case '(': sym = LPAR;  next_ch(); break;
      case ')': sym = RPAR;  next_ch(); break;
      case '[': sym = LSQBRA_SYM;  next_ch(); break; //ajoute
      case ']': sym = RSQBRA_SYM;  next_ch(); break;  //ajoute
      case '+': sym = PLUS;  next_ch(); break;
      case '-': sym = MINUS; next_ch(); break;
      case '<': next_ch();
      if(ch == '='){
        sym = LESS_THAN_EQUAL_SYM;  
        next_ch(); 
        break; //ajoute
      } else {
        sym = LESS_SYM;
      }
      case '>': next_ch(); 
      if(ch == '='){
        sym = GREATER_THAN_EQUAL_SYM;
        next_ch();
        break;
      } else {
        sym = GREATER_SYM;
      }
      break; //ajoute
      case '*': sym = MULT_SYM;  next_ch(); break; //ajoute
      case '/': sym = DIV_SYM;   next_ch(); break; //ajoute
      case '%': sym = PERCENT_SYM; next_ch(); break; //ajoute
      case ';': sym = SEMI;  next_ch(); break;
      case ':': sym = COLON_SYM;   next_ch(); break; //Ajout de ':'
      case '=': 
        next_ch();
      if(ch == '='){
        sym = EQUIVALENT_SYM;  //Ajout de '=='
        next_ch();
        break;
      } else {
        sym = EQUAL_SYM;
        break;
      }
      case '!': 
        next_ch();
        if(ch == '='){
          sym = NOT_EQUAL_SYM;
          next_ch();
          break;
        } else {
          syntax_error(0);
          break;
        }
      case EOF: sym = EOI;   next_ch(); break;
      default:
        if (ch >= '0' && ch <= '9')
          {
            int_val = 0; /* overflow? */

            while (ch >= '0' && ch <= '9')
              {
                count++;
                int_val = int_val*10 + (ch - '0');
                next_ch();
              }

            sym = INT;
          }
        else if (ch >= 'a' && ch <= 'z')
          {
            int i = 0; /* overflow? */

            while ((ch >= 'a' && ch <= 'z') || ch == '_')
              {
                count++;
                id_name[i++] = ch;
                next_ch();
              }

            id_name[i] = '\0';
            sym = 0;

            while (words[sym]!=NULL && strcmp(words[sym], id_name)!=0)
              sym++;

            if (words[sym] == NULL)
              {
                if (id_name[1] == '\0') sym = ID; else syntax_error(-1);
              }
          }
        else syntax_error(-1);
        
    }
}

/*---------------------------------------------------------------------------*/

/* Analyseur syntaxique. */

enum { VAR, CST, ADD, SUB, MUL, DIV, MOD,LT, GT, ASSIGN, EQ, GTEQ, LTEQ, NEQ,
       IF1, IF2, WHILE, DO, BREAK, CONTINUE, COLON, IGOTO, PRINT, EMPTY, SEQ, EXPR, PROG, LABEL }; //ajout du type de noeud PRINT

struct node
  {
    int kind;
    struct node *o1;
    struct node *o2;
    struct node *o3;
    int val;
  };

typedef struct node node;
node *root;

node *new_node(int k)
{
  node *x = malloc(sizeof(node));
  if(x == NULL){
    msg = "Erreur lors de l'allocation memoire.";
    longjmp(ptctrl, 1);
  }
  x->kind = k;
  x->o1 = NULL;
  x->o2 = NULL;
  x->o3 = NULL;
  return x;
}

node *paren_expr(); /* forward declaration */
node *mult(); //forward declaration

//node *freeAllMemory(); //forward declaration

node *term() /* <term> ::= <id> | <int> | <paren_expr> */
{
  node *x;

  if (sym == ID)           /* <term> ::= <id> */
    {
      x = new_node(VAR);
      x->val = id_name[0]-'a';
      next_sym();
    }
  else if (sym == INT)     /* <term> ::= <int> */
    {
      x = new_node(CST);
      x->val = int_val;
      next_sym();
    }
  else                     /* <term> ::= <paren_expr> */
    x = paren_expr();

  return x;
}

//<sum> ::= <term> dans le code mais <sum> ::= <mult> dans l'enonce du tp1?
node *sum() /* <sum> ::= <term>|<sum>"+"<term>|<sum>"-"<term> */
{
  node *x = mult();

  while (sym == PLUS || sym == MINUS)
    {
      node *t = x;
      x = new_node(sym==PLUS ? ADD : SUB);
      next_sym();
      x->o1 = t;
      x->o2 = mult();
    }

  return x;
}

node *mult() //Ajout de la regle <mult>
{
    node *x = term();
  if(sym == MULT_SYM || sym == DIV_SYM)
  {
    node *t = x;
    x = new_node(sym==MULT_SYM ? MUL : DIV);
    next_sym();
    x->o1 = t;
    x->o2 = mult();
  }
  else if(sym == PERCENT_SYM)
  {
    node *t = x;
    x = new_node(MOD);
    next_sym();
    x->o1 = t;
    x->o2 = mult();
  }
  return x;
}

//Ici on a modifie le code pour ajouter des fonctions demandees
//Possibilite d'optimiser le code
node *test() /* <test> ::= <sum> | <sum> "<" <sum> */
{
  node *x = sum();

  if (sym == LESS_SYM || sym == GREATER_SYM) //Ajout de comparaison stricte
    {
      node *t = x;
      x = new_node(sym == LESS_SYM? LT : GT); //Similaire au bloc du dessus avec *sum()
      next_sym();
      x->o1 = t;
      x->o2 = sum();
    }

  else if(sym == LESS_THAN_EQUAL_SYM || sym == GREATER_THAN_EQUAL_SYM) //Ajout de comparaison/egalite
  {
    node *t = x;
    x = new_node(sym == LESS_THAN_EQUAL_SYM? LTEQ : GTEQ); //Similaire au bloc du dessus avec *sum()
    next_sym();
    x->o1 = t;
    x->o2 = sum();
  }

  else if(sym == EQUIVALENT_SYM || sym == NOT_EQUAL_SYM) //Ajoute de l'egalite/difference
  {
    node *t = x;
    x = new_node(sym == EQUIVALENT_SYM? EQ : NEQ); //Similaire au bloc du dessus avec *sum()
    next_sym();
    x->o1 = t;
    x->o2 = sum();
  }
  return x;
}

node *expr() /* <expr> ::= <test> | <id> "=" <expr> */
{
  
  node *x;
   x = test();
  if (sym == EQUAL_SYM)
    {
      next_sym();
      node *t = x;
      if(sym == EQUAL_SYM){
        x = new_node(EQUIVALENT_SYM);
        next_sym();
        x->o1 = t;
        x->o2 = sum();
      } else{
        x = new_node(ASSIGN);
        x->o1 = t;
        x->o2 = expr();
      }
    }
  return x;
}

node *paren_expr() /* <paren_expr> ::= "(" <expr> ")" */
{
  node *x;

  if (sym == LPAR) next_sym(); else syntax_error(9);

  x = expr();

  if (sym == RPAR) next_sym(); else syntax_error(2);

  return x;
}

//Ici on a modifie le code pour ajouter des fonctions demandees
//Possibilite d'optimiser le code
node *statement()
{
  node *x;

  if (sym == IF_SYM)       /* "if" <paren_expr> <stat> */
    {
      x = new_node(IF1);
      next_sym();
      x->o1 = paren_expr();
      x->o2 = statement();
      if (sym == ELSE_SYM) /* ... "else" <stat> */
        { x->kind = IF2;
          next_sym();
          x->o3 = statement();
        }
    }
  else if (sym == WHILE_SYM) /* "while" <paren_expr> <stat> */
    {
      x = new_node(WHILE);
      next_sym();
      x->o1 = paren_expr();
      x->o2 = statement();
    }
  else if (sym == DO_SYM)  /* "do" <stat> "while" <paren_expr> ";" */
    {
      x = new_node(DO);
      next_sym();
      x->o1 = statement();
      if (sym == WHILE_SYM) next_sym(); else syntax_error(9);
      x->o2 = paren_expr();
      if (sym == SEMI) next_sym(); else syntax_error(7);
    }
  else if (sym == SEMI)    /* ";" */
    {
      x = new_node(EMPTY);
      next_sym();
    }
  else if (sym == LBRA)    /* "{" { <stat> } "}" */
    {
      x = new_node(EMPTY);
      next_sym();
      while (sym != RBRA)
        {
          node *t = x;
          x = new_node(SEQ);
          x->o1 = t;
          x->o2 = statement();
        }
      next_sym();
    }
    else if(sym == PRINT_SYM)   /* Ajout "print" <paren_expr> ";" */
    {
      x = new_node(PRINT);
      next_sym();
      x->o1 = paren_expr();
      if(sym == SEMI) next_sym(); else syntax_error(7);
    }
    
    /*"break/continue" [ <id> ] ";"*/
    else if(sym == BREAK_SYM || sym == CONTINUE_SYM) 
    {
      x = new_node(sym==BREAK_SYM? BREAK : CONTINUE);
      next_sym();
      if (sym != SEMI) x->o1 = expr();
      else x->o1 = new_node(EMPTY);
      if (sym == SEMI) next_sym(); else syntax_error(7);// Checks for ";"
    }
    else if(sym == GOTO_SYM) /* Ajout "goto" <id> ";" */
    {
      x = new_node(IGOTO);
      next_sym();
      x->o1 = expr();
      if(sym == SEMI) next_sym(); else syntax_error(7);
    }
    else                     /* <expr> ";" */
    {
      
      x = new_node(EXPR);
      x->o1 = expr();
      /* Ajout <id>":"<stat> */
        if(sym == COLON_SYM){
          x->kind = LABEL;
          next_sym();
          x->o2 = statement();
        }
        else if (sym == SEMI) next_sym(); else syntax_error(7);
    }
  return x;
}

node *program()  /* <program> ::= <stat> */
{
  node *x = new_node(PROG);
  next_sym();
  x->o1 = statement();
  if (sym != EOI) syntax_error(-1);
  return x;
}

/*---------------------------------------------------------------------------*/

/* Analyseur de memoire */

  //Libere tous les sous arbres a tout moment
  //Appele depuis la racine/noeud racine
  //Traverse en postfixe
  void freeNode(node *node)
  {
    //Cas de base
//    if (node->o1 == NULL && node->o2 == NULL && node->o3 == NULL){
//       return;
//    }
    if(node->o1 == NULL && node->o2 == NULL && node->o3 == NULL){
      return;
    }
    /* Parcours en post-ordre */
    /* puis supprime le noeud si toutes ses references sont nulles */
        if(node->o1 != NULL){freeNode(node->o1);}
        if(node->o2 != NULL){freeNode(node->o2);}
        if(node->o3 != NULL){freeNode(node->o3);}
    node->kind = NULL;
    free(node);
  }

  /* Supprime l'arbre ASA et sa racine */
  void deleteTree()
  {
    if(root != NULL ){
            freeNode(root);
      	root = NULL; //reference de la racine
        free(root);
    }
  }

/* Generateur de code. */

enum { ILOAD, ISTORE, BIPUSH, DUP, POP, IADD, ISUB, IMUL, IMOD, IDIV, IFLT, IFGT, IFLTEQ, IFGTEQ,
       GOTO, IFEQ, IFNEQ, IPRINT, SLABEL, LLABEL, RETURN };

typedef signed char code;

code object[1000], *here = object;
code *labels[26];
code *jump[255], jump_ptr = 0;
code *branch_stack[255], branch_pt = 0;
code *brk_stack[255], break_here = 0;
code *cont_stack[255], cont_here = 0;
int thisBoucle = 27;

void gen(code c) { *here++ = c; if (here-object>1000){ msg="\n Erreur : Object pointer overflow"; longjmp(ptctrl, 2);}} 

void setLoops(code *debut, code *fin);
void setJumpPoints(code *jump, int step, code *jumpingPoint[]);

#define SHOW_CODE // pour debugger
#ifdef SHOW_CODE
#define g(c) do { printf(" %d",c); gen(c); } while (0)
#define gi(c) do { printf("\n%s", #c); gen(c); } while (0)
#else
#define g(c) gen(c)
#define gi(c) gen(c)
#endif

void fix(code *src, code *dst) { *src = dst-src; if ((src-object)>128 || (src-object)<-127){ msg="\nBranchement a l'indice de destination est hors limites"; longjmp(ptctrl, 2);}} /* overflow? */

void c(node *x)
{ switch (x->kind)
    { case VAR   : gi(ILOAD); g(x->val); break;

      case CST   : gi(BIPUSH); g(x->val);  break;

      case ADD   : c(x->o1); c(x->o2); gi(IADD);  break;

      case DIV   : c(x->o1); c(x->o2); gi(IDIV);  break;

      case SUB   : c(x->o1); c(x->o2); gi(ISUB);   break;
      
      case MUL   : c(x->o1); c(x->o2); gi(IMUL);  break;
      
      case MOD   : c(x->o1); c(x->o2); gi(IMOD); break;

      case EQ    : gi(BIPUSH); g(1);
                   c(x->o1);
                   c(x->o2);
                   gi(ISUB);
                   gi(IFEQ); g(4);
                   gi(POP); 
                   gi(BIPUSH); g(0); 
                   break;

      case LT    : gi(BIPUSH); g(1);
                   c(x->o1);
                   c(x->o2);
                   gi(ISUB);
                   gi(IFLT); g(4);
                   gi(POP);
                   gi(BIPUSH); g(0); 
                   break;

      case GT    : gi(BIPUSH); g(1);  //ajout de GREATER THAN
                   c(x->o1);
                   c(x->o2);
                   gi(ISUB);
                   gi(IFGT); g(4);
                   gi(POP);
                   gi(BIPUSH); g(0); 
                   break;
                  
      case LTEQ  : gi(BIPUSH); g(1);  //ajout de LESS THAN EQUAL
                   c(x->o1);
                   c(x->o2);
                   gi(ISUB);
                   gi(IFLTEQ); g(4);
                   gi(POP);
                   gi(BIPUSH); g(0); 
                   break;
          
      case GTEQ    : gi(BIPUSH); g(1);  //ajout de GREATER THAN EQUAL
                   c(x->o1);
                   c(x->o2);
                   gi(ISUB);
                   gi(IFGTEQ); g(4);
                   gi(POP);
                   gi(BIPUSH); g(0); 
                   break;
      
      case NEQ   : gi(BIPUSH); g(1);   //Ajout de NOT EQUAL
                   c(x->o1);
                   c(x->o2);
                   gi(ISUB);
                   gi(IFNEQ); g(4);
                   gi(POP);
                   gi(BIPUSH); g(0);
                    break;

      case ASSIGN: c(x->o2);
                   gi(DUP);
                   gi(ISTORE); g(x->o1->val); 
                    break;

      case IF1   : { code *p1;
                     c(x->o1);
                     gi(IFEQ); p1 = here++;
                     c(x->o2); fix(p1,here); break;
                   }

      case IF2   : { code *p1, *p2;
                     c(x->o1);
                     gi(IFEQ); p1 = here++;
                     c(x->o2);
                     gi(GOTO); p2 = here++; fix(p1,here);
                     c(x->o3); fix(p2,here); break;
                   }

      case WHILE : { code *p1 = here, *p2; 
                     thisBoucle++;
                     if(thisBoucle > 128 || thisBoucle <= -1){
                      msg = "Erreur lors de l'evaluation de la boucle while";
                      longjmp(ptctrl, 2);
                    }
                     c(x->o1);
                     gi(IFEQ); p2 = here++;
                     c(x->o2);
                     gi(GOTO); fix(here++,p1); fix(p2,here);
                     setLoops(p1,here); 
                     thisBoucle--; break;
                   }

      case DO    : { code *p1 = here; c(x->o1); 
                     thisBoucle++;
                    if(thisBoucle > 128 || thisBoucle <= -1){
                      msg = "Erreur lors de l'evaluation de la boucle do while";
                      longjmp(ptctrl, 2);
                    }
                     c(x->o2);
                     gi(IFNEQ); fix(here++,p1);
                     setLoops(p1, here);
                    thisBoucle++;
                    break;
                   }
      case CONTINUE : {
                        if(thisBoucle > 27 ){
                            msg = "Erreur, impossible de poursuivre la boucle";
                            longjmp(ptctrl, 2);
                        }
                        gi(GOTO);
                        if(x->o1->kind == EMPTY){
                          cont_stack[cont_here] = here++;
                          *cont_stack[cont_here] = x->o1->val;
                        } else {
                          cont_stack[cont_here] = here++;
                          *cont_stack[cont_here] = thisBoucle;
                        }
                        break;
                      }
      case LABEL :{
                  if(labels[x->o1->val] != NULL)
                  {
                     msg = "Erreur lors de l'assignation des labels : Label deja assigne."; longjmp(ptctrl, 2);
                    break;
                  } else {
                    labels[x->o1->val] = here;
                    c(x->o2); break;
                  }
              }

      case IGOTO :   {
                      gi(GOTO);
                      branch_stack[branch_pt] = here++;
                      *branch_stack[branch_pt++] = x->o1->val;
                      break;
                    }
                                            
      case EMPTY : 
                    break;

      case SEQ   : c(x->o1);
                   c(x->o2); 

                  break;

      case EXPR  : c(x->o1);
                   gi(POP); 

                   break;
                  
      case PRINT : c(x->o1);
                   gi(IPRINT); 
                  break;

      case PROG  : c(x->o1);
                   gi(RETURN); 

                  break;
      
      case BREAK:
                if(x->o1->kind != EMPTY){
                  brk_stack[break_here] = ++here;
                  *brk_stack[break_here++] = x->o1->val;
                } else {
                  brk_stack[break_here] = here++;
                  *brk_stack[break_here] = thisBoucle;
                }
          break;
    }
}

void creerLabel(){
  for(int i=0; i<jump_ptr; i++){
    if(labels[*jump[i]] == NULL) {
      msg = "Erreur : Aucun label n'existe a ce nom."; longjmp(ptctrl, 1);
    } else{
      fix(*jump[i], labels[*jump[i]]);
    }
  }
}

//Initialise les adresses de saut dans les etiquettes/break/goto/continue
void setJumpPoints(code *jump, int step, code *jumpingPoint[]){
  for(int i=0; i<step; i++){
    //Verifie pour les break/continue
    if(*jumpingPoint[i] == 0){
      msg = "Erreur : Point de branchement en dehors d'une boucle"; longjmp(ptctrl, 1);
    }else if (jumpingPoint[i] == NULL) {
      continue;
    }else if (*jumpingPoint[i] == thisBoucle) {
      fix(jumpingPoint[i], jump);
      jumpingPoint[i] = NULL; //On a consume ce break de branchement (empecher de boucler sur le meme break en boucle)
    }else {
      //Verifie pour les etiquettes
      for (int e = 0; e < 27;e++) {
        if(*jumpingPoint[i] == (*labels[e]-'a')){
          fix(jumpingPoint[i],jump);
          jumpingPoint[i] == NULL;
        }
      }
    }
  }
}

void setLoops(code *debut, code *fin){
  setJumpPoints(debut, cont_here, cont_stack);
  setJumpPoints(fin, break_here, brk_stack);
  
  if(thisBoucle > 27){
    int i = 0;
    int k = 0;
    while(break_here<cont_here){
      if((brk_stack[i] != NULL && i < break_here) || cont_stack[k] != NULL && k < cont_here){
        msg = "Erreur : Point de branchement en dehors d'une boucle"; longjmp(ptctrl, 2);
      }
      i++;
      k++;
    }
  }
}


/*---------------------------------------------------------------------------*/

/* Machine virtuelle. */

int globals[26];


void run()
{
  int stack[1000], *sp = stack; /* overflow? */
  code *pc = object;
  
  for (;;){
    switch (*pc++)
      {
        case ILOAD : *sp++ = globals[*pc++];             break;
        case ISTORE: globals[*pc++] = *--sp;             break;
        case BIPUSH: *sp++ = *pc++;                      break;
        case DUP   : sp++; sp[-1] = sp[-2];              break;
        case POP   : --sp;                               break;
        case IADD  : sp[-2] = sp[-2] + sp[-1]; --sp;     break;
        case ISUB  : sp[-2] = sp[-2] - sp[-1]; --sp;     break;
        case IMUL  : sp[-2] = sp[-2] * sp[-1]; --sp;     break; //Ajout pour instruc MULT
        case IDIV  : if (sp[-1] != 0)   
                      {
                          sp[-2] = sp[-2] / sp[-1]; --sp;     break; 
                      }
                      else                                        
                      {
                          msg = "Erreur division par 0"; longjmp(ptctrl, 3);
                      }
                        break;
        case IMOD : if (sp[-1] != 0)
                      {
                        sp[-2] = sp[-2] % sp[-1]; --sp;    break;                      
                      }
                      else   
                      {
                          msg = "Erreur division par 0"; longjmp(ptctrl, 3);
                      }
                        break;
        case IPRINT: printf("%d\n", *--sp);              break; //Ajout pour instruc PRINT
        case GOTO  :  pc += *pc;                         break;
        case IFEQ  : if (*--sp==0) pc += *pc; else pc++; break;
        case IFNEQ : if (*--sp!=0) pc += *pc; else pc++; break;
        case IFLT  : if (*--sp< 0) pc += *pc; else pc++; break;
        case IFGT  : if (*--sp> 0) pc += *pc; else pc++; break; //Ajout pour instruc GT
        case IFLTEQ  : if (*--sp<= 0) pc += *pc; else pc++; break; //Ajout pour instruc IFLTEQ
        case IFGTEQ  : if (*--sp>= 0) pc += *pc; else pc++; break; //Ajout pour instruc IFGTEQ
        case RETURN: return;
        
    }
    if(sp-stack > 1000){ 
      msg = "Erreur : Stack overflow"; longjmp(ptctrl, 3);
    } /* Checks for stack overflow */
  }
}

/*---------------------------------------------------------------------------*/

/* Programme principal. */

int main()
{
  int i;
  
    if (setjmp(ptctrl) == 0) {  
      for (i=0; i<26; i++){ //initialise les labels a NULL et les variables a 0
        globals[i] = 0;
        labels[i] = NULL;
      }
      
      //racine de l'ASA
       root = program();
      c(root);
      creerLabel();
    #ifdef SHOW_CODE
      printf("\n");
    #endif
      run();
      deleteTree();
      return 0;
      
    } else {
        deleteTree();
//        switch () {
//        	case 1 : printf("Erreur memoire\n");break;
//          case 2 : printf("Erreur de compilation\n"); break;
//          case 3: printf("Erreur logique\n");break; 
//          default: break;
//        }
        printf("%s",msg);
        exit(1);
    }
}

/*---------------------------------------------------------------------------*/